Sample C++20 Vulkan Cube app for MacOS & Linux written utilizing Unix programming style and clang-13. 

![Vulkan Cube](cube.mov)

Linux libs:
```sh  
sudo pacman -S zsh clang lld lldb valgrind b3sum
sudo pacman -S vulkan-intel
# or
sudo pacman -S vulkan-nouveau
# or
sudo pacman -S vulkan-radeon
sudo dnf install zsh clang lld lldb libasan-devel libubsan-devel lib64mesagl1-devel valgrind lib64clang-devel b3sum
sudo apt install zsh clang lld lldb libasan8 libubsan1 libgl1-mesa-dev valgrind libclang-dev b3sum  
```

## Debian Linux Setup
```sh
wget https://sdk.lunarg.com/sdk/download/1.3.283.0/linux/vulkansdk-linux-x86_64-1.3.283.0.tar.xz 
```
In ~/.bashrc or ~/.zshrc: 
```sh
source .../vulkansdk-linux-x86_64-1.3.283.0/setup-env.sh 
```

Make symlink to SDK: 
```sh
sudo ln -s .../vulkansdk-linux-x86_64-1.3.283.0 /opt/vulkansdk 
```

Compile glfw on debian (https://github.com/glfw/glfw/releases, https://www.glfw.org/docs/latest/compile.html)  
```sh
cd glfw-3.4  
cmake -S . -B ./build 
cd build 
make 

ls build/src/libglfw3.a 
```

Compile safeclib on debian (https://github.com/rurban/safeclib/releases)  
```sh
./configure 
make 
sudo make install 

ls /usr/local/lib/libsafec.a 
```

Compile portaudio on debian (https://github.com/PortAudio/portaudio/releases)  
```sh
./configure 
make 
sudo make install 

ls /usr/local/lib/libportaudio.a 
```

Run:  
```sh
zsh compile.sh -f
```

## MacOS Setup

Install vulkansdk-macos-1.3.283.0.dmg with "System Global Installation" flag.  

Compile safeclib on MacOS (https://github.com/rurban/safeclib/releases)  
```sh
./configure --disable-hardening CC="clang -arch arm64 -arch x86_64" \
  CXX="clang -arch arm64 -arch x86_64" CPP="clang -E" CXXCPP="clang -E"
make
sudo make install

ls /usr/local/lib/libsafec.a
```

Compile portaudio on MacOS (https://github.com/PortAudio/portaudio/releases)  
```sh
./configure --disable-mac-universal
make
sudo make install

ls /usr/local/lib/libportaudio.a
```

No need to compile glfw for MacOS. Github has precompiled binary. (https://github.com/glfw/glfw/releases, https://www.glfw.org/docs/latest/compile.html)  

Run:  
```sh
zsh compile.sh -f
```
