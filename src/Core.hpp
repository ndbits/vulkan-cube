#pragma once
#pragma clang diagnostic ignored "-Wswitch"

#ifdef __MACH__
	#include "vk_mvk_moltenvk.h"
#elif __linux__ || __CYGWIN__ || _WIN64 || __MINGW32__
	#include "vulkan/vulkan.hpp"
#endif

#define GLFW_INCLUDE_VULKAN
#include "GLFW/glfw3.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include "glm.hpp"
#include "gtc/type_ptr.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtx/hash.hpp"

#include "safe_mem_lib.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdexcept>
#include <chrono>
#include <vector>
#include <tuple>
#include <cstdlib>
#include <cstdint>
#include <array>
#include <unordered_map>
#include <set>
#include <utility>
#include <algorithm>
#include <optional>
#include <thread>

using std::cout;
using std::endl;
using std::cerr;
using std::string;
using std::array;
using std::vector;
using std::tuple;
using std::make_tuple;
using std::get;
using std::unordered_map;
using std::runtime_error;
using std::cerr;
using std::exception;
using std::set;
using std::invalid_argument;
using std::pair;
using std::make_pair;
using std::reference_wrapper;
using std::unwrap_reference;
using std::optional;
using std::thread;

using glm::mat4;
using glm::vec3;
using glm::vec2;
using glm::radians;
using glm::lookAt;
using glm::perspective;
using glm::rotate;
using glm::translate;
using glm::clamp;
using glm::length;
using glm::normalize;
using glm::all;
using glm::equal;
using glm::epsilonEqual;

class Core {
public:
/**
	@short Reads files to create vector
	@param const char *filename
	@return vector<char>
 */
	static vector<char> read_file(const char *filename);

private:
};