#pragma once

#include "Image.hpp"

struct SwapchainSupportDetails {
	VkSurfaceCapabilities2KHR capabilities;
	vector<VkSurfaceFormat2KHR> formats;
	vector<VkPresentModeKHR> present_modes;
};

class Graphics {
public:

	static pair<VkInstance &, GLFWwindow *> create_instance(void);

	static pair<VkQueue &, VkQueue &> get_graphics_present_queues(const VkDevice &logical_device);
	static VkSurfaceKHR & create_surface(const VkInstance &instance, GLFWwindow *window);

	static SwapchainSupportDetails & get_swapchain_details(const VkPhysicalDevice &physical_device);
	static tuple<VkSwapchainKHR, vector<VkImage> &, vector<VkImageView> &, ImageTriple &> create_swapchain(const VkDevice &logical_device);
	static VkExtent2D & choose_swapchain_extent(const VkSurfaceCapabilities2KHR &capabilities);

	static pair<VkPipeline &, VkPipelineLayout &> create_pipeline(const VkPhysicalDevice &physical_device, const VkDevice &logical_device, const vector<VkDescriptorSetLayout> &descriptor_layouts);

private:

	static VkSurfaceFormat2KHR choose_surface_format(const vector<VkSurfaceFormat2KHR> &formats);
	static VkPresentModeKHR choose_swapchain_present_mode(const vector<VkPresentModeKHR> &present_mods);

	static VkShaderModule create_shader_module(const VkDevice &logical_device, const vector<char> &code);

};