#include "Graphics.hpp"

static SwapchainSupportDetails swapchain_details {};
static VkSwapchainKHR swapchain {};
static VkExtent2D swapchain_extent {};

static VkSurfaceKHR surface;

static VkPipeline graphics_pipeline {};
static VkPipelineLayout pipeline_layout {};

static VkQueue graphics_queue {};
static VkQueue present_queue {};

static vector<VkImage> swapchain_images {};
static vector<VkImageView> swapchain_image_views {};
static vector<VkFramebuffer> swapchain_framebuffers {};

static ImageTriple swapchain_depth_tripple {};

static VkInstance instance {};
static GLFWwindow *window {};

const GLint win_width {800};
const GLint win_height {600};

pair<VkInstance &, GLFWwindow *> Graphics::create_instance() {
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	window = glfwCreateWindow(win_width, win_height, "Cubes", nullptr, nullptr);

	if ( !window) {
		printf("Failed to create GLFW window!\n");
		glfwTerminate();
		exit(1);
	}
	glfwMakeContextCurrent(window);

	VkApplicationInfo app_info {};
	app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	app_info.pApplicationName = "Cubes";
	app_info.applicationVersion = VK_MAKE_VERSION(2, 1, 0);
	app_info.pEngineName = "Mageia";
	app_info.engineVersion = VK_MAKE_VERSION(2, 1, 0);
	app_info.apiVersion = VK_MAKE_API_VERSION(0, 1, 3, 0);

	VkInstanceCreateInfo create_info {};
	create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	create_info.flags = VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR; // Required since v1.3.224
	create_info.pApplicationInfo = &app_info;
	create_info.enabledLayerCount = 1;

	uint32_t glfw_ext_count = 0;
	const char **glfw_extensions{ glfwGetRequiredInstanceExtensions(&glfw_ext_count) };

	vector<const char *> extensions_enabled {glfw_extensions, glfw_extensions + glfw_ext_count};
	extensions_enabled.push_back(VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME); // Required since v1.3.224
	extensions_enabled.push_back(VK_KHR_GET_SURFACE_CAPABILITIES_2_EXTENSION_NAME); // VK_KHR_get_surface_capabilities2

	if (enable_validation_layers) {
		extensions_enabled.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	create_info.enabledExtensionCount = static_cast<GLuint>(extensions_enabled.size());
	create_info.ppEnabledExtensionNames = extensions_enabled.data();

	VkValidationFeaturesEXT validation_features_ext {};

	if (enable_validation_layers) {
		create_info.enabledLayerCount = static_cast<GLuint>(validation_layers.size());
		create_info.ppEnabledLayerNames = validation_layers.data();

		validation_features_ext.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
		validation_features_ext.pNext = create_info.pNext;
		validation_features_ext.enabledValidationFeatureCount = static_cast<GLuint>(sizeof(validation_features)/sizeof(validation_features[0]));
		validation_features_ext.pEnabledValidationFeatures = (VkValidationFeatureEnableEXT *) &validation_features;
		validation_features_ext.disabledValidationFeatureCount = static_cast<GLuint>(sizeof(disabled_validation_features)/sizeof(disabled_validation_features[0]));
		validation_features_ext.pDisabledValidationFeatures = (VkValidationFeatureDisableEXT *) &disabled_validation_features;
	} else {
		create_info.enabledLayerCount = 0;
		create_info.pNext = nullptr;
	}

	Debug::vkresult_check(
		vkCreateInstance(&create_info, nullptr, &instance),
	__FILE__, __LINE__, __FUNCTION__);

	return make_pair(reference_wrapper(instance), window);
}

pair<VkQueue &, VkQueue &> Graphics::get_graphics_present_queues(const VkDevice &logical_device) {
	QueueFamilyIndices queue_indices {MemDev::get_queue_families(surface)};
	vkGetDeviceQueue(logical_device, queue_indices.graphicsFamily.value(), 0, &graphics_queue);
	vkGetDeviceQueue(logical_device, queue_indices.presentFamily.value(), 0, &present_queue);
	return make_pair(reference_wrapper(graphics_queue), reference_wrapper(present_queue));
}

VkSurfaceKHR & Graphics::create_surface(const VkInstance &instance, GLFWwindow *window) {
	if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
		throw runtime_error("failed to create window surface!");
	}
	return surface;
}

tuple<VkSwapchainKHR, vector<VkImage> &, vector<VkImageView> &, ImageTriple &> Graphics::create_swapchain(const VkDevice &logical_device) {

	VkSurfaceFormat2KHR surface_format {choose_surface_format(swapchain_details.formats)};
	VkPresentModeKHR present_mode {choose_swapchain_present_mode(swapchain_details.present_modes)};
	swapchain_extent = choose_swapchain_extent(swapchain_details.capabilities);

	GLuint image_count {swapchain_details.capabilities.surfaceCapabilities.minImageCount + 1}; /* 3 */
	if (swapchain_details.capabilities.surfaceCapabilities.maxImageCount > 0 && image_count > swapchain_details.capabilities.surfaceCapabilities.maxImageCount) {
		image_count = swapchain_details.capabilities.surfaceCapabilities.maxImageCount;
	}
	VkSwapchainCreateInfoKHR swapchain_create_info {};
		swapchain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapchain_create_info.surface = surface;
		swapchain_create_info.minImageCount = image_count;
		swapchain_create_info.imageFormat = surface_format.surfaceFormat.format;
		swapchain_create_info.imageColorSpace = surface_format.surfaceFormat.colorSpace;
		swapchain_create_info.imageExtent = swapchain_extent;
		swapchain_create_info.imageArrayLayers = 1;
		swapchain_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	QueueFamilyIndices graphics_families {MemDev::get_queue_families(surface)};
	GLuint family_set[] = {graphics_families.graphicsFamily.value(), graphics_families.presentFamily.value()};

	if (graphics_families.graphicsFamily != graphics_families.presentFamily) {
		swapchain_create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		swapchain_create_info.queueFamilyIndexCount = 2;
		swapchain_create_info.pQueueFamilyIndices = family_set;
	} else {
		swapchain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}

	swapchain_create_info.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchain_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchain_create_info.presentMode = present_mode;
	swapchain_create_info.clipped = VK_TRUE;

	Debug::vkresult_check(
		vkCreateSwapchainKHR(logical_device, &swapchain_create_info, nullptr, &swapchain),
	__FILE__, __LINE__, __FUNCTION__);

	/* Creating all swapchain images and views with depth */
	vkGetSwapchainImagesKHR(logical_device, swapchain, &image_count, nullptr);
	swapchain_images.resize(image_count);
	vkGetSwapchainImagesKHR(logical_device, swapchain, &image_count, swapchain_images.data());

	for (size_t i = 0; i < image_count; i += 1) {
		swapchain_image_views.push_back(
			Image::create_image_view(
				logical_device,
				swapchain_images[i],
				surface_format.surfaceFormat.format,
				VK_IMAGE_ASPECT_COLOR_BIT
		));
	}

	swapchain_depth_tripple = Image::create_depthimage(logical_device, swapchain_extent.width, swapchain_extent.height);

	return make_tuple(swapchain, reference_wrapper(swapchain_images), reference_wrapper(swapchain_image_views), reference_wrapper(swapchain_depth_tripple));
}

pair<VkPipeline &, VkPipelineLayout &> Graphics::create_pipeline(const VkPhysicalDevice &physical_device, const VkDevice &logical_device, const vector<VkDescriptorSetLayout> &descriptor_layouts) {

	vector<char> vert_shader_code {Core::read_file("shaders/depth_vert.spv")};
	vector<char> frag_shader_code {Core::read_file("shaders/depth_frag.spv")};

	VkShaderModule vert_shader_module = create_shader_module(logical_device, vert_shader_code);
	VkShaderModule frag_shader_module = create_shader_module(logical_device, frag_shader_code);

	VkPipelineShaderStageCreateInfo vert_shader_stage_info {};
		vert_shader_stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vert_shader_stage_info.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vert_shader_stage_info.module = vert_shader_module;
		vert_shader_stage_info.pName = "main";

	VkPipelineShaderStageCreateInfo frag_shader_stage_info {};
		frag_shader_stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		frag_shader_stage_info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		frag_shader_stage_info.module = frag_shader_module;
		frag_shader_stage_info.pName = "main";

	VkPipelineShaderStageCreateInfo shader_stages[] = {frag_shader_stage_info, vert_shader_stage_info};

	VkPipelineVertexInputStateCreateInfo vertex_input_info {};
	vertex_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

	auto binding_description = Vertex::get_binding_description();
	auto attribute_descriptions = Vertex::get_attribute_descriptions();

	vertex_input_info.vertexBindingDescriptionCount = 1;
	vertex_input_info.vertexAttributeDescriptionCount = static_cast<GLuint>(attribute_descriptions.size());
	vertex_input_info.pVertexBindingDescriptions = &binding_description;
	vertex_input_info.pVertexAttributeDescriptions = attribute_descriptions.data();

	VkPipelineInputAssemblyStateCreateInfo input_assembly {};
	input_assembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	input_assembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	input_assembly.primitiveRestartEnable = VK_FALSE;

	swapchain_details = get_swapchain_details(physical_device);
	swapchain_extent = choose_swapchain_extent(swapchain_details.capabilities);

	VkViewport viewport {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (GLfloat) swapchain_extent.width;
		viewport.height = (GLfloat) swapchain_extent.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

	VkRect2D scissor {};
		scissor.offset = {0, 0};
		scissor.extent = swapchain_extent;

	VkPipelineViewportStateCreateInfo viewport_state {};
		viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewport_state.viewportCount = 1;
		viewport_state.pViewports = &viewport;
		viewport_state.scissorCount = 1;
		viewport_state.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer {};
		rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizer.depthClampEnable = VK_FALSE;
		rasterizer.rasterizerDiscardEnable = VK_FALSE;
		rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
		rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling {};
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampling.sampleShadingEnable = VK_FALSE;
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	VkPipelineDepthStencilStateCreateInfo depth_stencil {};
		depth_stencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depth_stencil.depthTestEnable = VK_TRUE;
		depth_stencil.depthWriteEnable = VK_TRUE;
		depth_stencil.depthCompareOp = VK_COMPARE_OP_LESS;
		depth_stencil.depthBoundsTestEnable = VK_FALSE;
		depth_stencil.stencilTestEnable = VK_FALSE;

	VkPipelineColorBlendAttachmentState color_blend_attachment {};
		color_blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		color_blend_attachment.blendEnable = VK_TRUE;
		color_blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		color_blend_attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		color_blend_attachment.colorBlendOp = VK_BLEND_OP_ADD;
		color_blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		color_blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		color_blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo color_blending {};
		color_blending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		color_blending.logicOpEnable = VK_FALSE;
		color_blending.logicOp = VK_LOGIC_OP_COPY;
		color_blending.attachmentCount = 1;
		color_blending.pAttachments = &color_blend_attachment;
		color_blending.blendConstants[0] = 0.0f;
		color_blending.blendConstants[1] = 0.0f;
		color_blending.blendConstants[2] = 0.0f;
		color_blending.blendConstants[3] = 0.0f;

	VkPipelineLayoutCreateInfo pipeline_layout_info {};
		pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipeline_layout_info.setLayoutCount = descriptor_layouts.size();
		pipeline_layout_info.pSetLayouts = descriptor_layouts.data();

	Debug::vkresult_check(
		vkCreatePipelineLayout(logical_device, &pipeline_layout_info, nullptr, &pipeline_layout),
	__FILE__, __LINE__, __FUNCTION__);

	VkPipelineRenderingCreateInfo pipeline_rendering_create_info {};
		pipeline_rendering_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO;
		pipeline_rendering_create_info.colorAttachmentCount = 1;
		VkFormat swapchain_image_format {VK_FORMAT_B8G8R8A8_SRGB}; // TODO: surface_format.surfaceFormat.format;
		pipeline_rendering_create_info.pColorAttachmentFormats = &swapchain_image_format;
		pipeline_rendering_create_info.depthAttachmentFormat = MemDev::get_depth_stencil_format();
		pipeline_rendering_create_info.stencilAttachmentFormat = MemDev::get_depth_stencil_format();

	VkGraphicsPipelineCreateInfo pipeline_info {};
		pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipeline_info.pNext = &pipeline_rendering_create_info;
		pipeline_info.stageCount = sizeof(shader_stages) / sizeof(shader_stages[0]);
		pipeline_info.pStages = shader_stages;
		pipeline_info.pVertexInputState = &vertex_input_info;
		pipeline_info.pInputAssemblyState = &input_assembly;
		pipeline_info.pViewportState = &viewport_state;
		pipeline_info.pRasterizationState = &rasterizer;
		pipeline_info.pMultisampleState = &multisampling;
		pipeline_info.pDepthStencilState = &depth_stencil;
		pipeline_info.pColorBlendState = &color_blending;
		pipeline_info.layout = pipeline_layout;

	Debug::vkresult_check(
		vkCreateGraphicsPipelines(logical_device, VK_NULL_HANDLE, 1, &pipeline_info, nullptr, &graphics_pipeline),
	__FILE__, __LINE__, __FUNCTION__);

	vkDestroyShaderModule(logical_device, frag_shader_module, nullptr);
	vkDestroyShaderModule(logical_device, vert_shader_module, nullptr);

	return make_pair(reference_wrapper(graphics_pipeline), reference_wrapper(pipeline_layout));
}

SwapchainSupportDetails & Graphics::get_swapchain_details(const VkPhysicalDevice &physical_device) {
	swapchain_details.capabilities.sType = VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR;

	VkPhysicalDeviceSurfaceInfo2KHR surface_info {};
		surface_info.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR;
		surface_info.surface = surface;

	vkGetPhysicalDeviceSurfaceCapabilities2KHR(physical_device, &surface_info, &swapchain_details.capabilities);

	GLuint format_count;
	vkGetPhysicalDeviceSurfaceFormats2KHR(physical_device, &surface_info, &format_count, nullptr);

	if (format_count != 0) {
		swapchain_details.formats.resize(format_count);
		for (auto &format : swapchain_details.formats) {
				format.sType = VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR;
		}
		vkGetPhysicalDeviceSurfaceFormats2KHR(physical_device, &surface_info, &format_count, swapchain_details.formats.data());
	}

	GLuint present_mode_count;
	vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &present_mode_count, nullptr);

	if (present_mode_count != 0) {
		swapchain_details.present_modes.resize(present_mode_count);
		vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &present_mode_count, swapchain_details.present_modes.data());
	}

	return swapchain_details;
}

VkSurfaceFormat2KHR Graphics::choose_surface_format(const vector<VkSurfaceFormat2KHR> &formats) {
	for (const auto &format : formats) {
		if (format.surfaceFormat.format == VK_FORMAT_B8G8R8A8_SRGB && format.surfaceFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return format;
		}
	}

	return formats[0];
}

VkPresentModeKHR Graphics::choose_swapchain_present_mode(const vector<VkPresentModeKHR> &present_mods) {
	for (const auto &mode : present_mods) {
		if (mode == VK_PRESENT_MODE_MAILBOX_KHR) {
			return mode;
		}
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D & Graphics::choose_swapchain_extent(const VkSurfaceCapabilities2KHR &capabilities) {
	if (capabilities.surfaceCapabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		swapchain_extent = capabilities.surfaceCapabilities.currentExtent;
	} else {
		int fb_width, fb_height;
        glfwGetFramebufferSize(window, &fb_width, &fb_height);
		swapchain_extent = {
			static_cast<GLuint>(fb_width),
			static_cast<GLuint>(fb_height)
		};

		swapchain_extent.width = clamp(swapchain_extent.width, capabilities.surfaceCapabilities.minImageExtent.width, capabilities.surfaceCapabilities.maxImageExtent.width);
		swapchain_extent.height = clamp(swapchain_extent.height, capabilities.surfaceCapabilities.minImageExtent.height, capabilities.surfaceCapabilities.maxImageExtent.height);
	}

	return swapchain_extent;
}

VkShaderModule Graphics::create_shader_module(const VkDevice &logical_device, const vector<char> &code) {
	VkShaderModuleCreateInfo shader_module_create_info {};
	shader_module_create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shader_module_create_info.codeSize = code.size();
	shader_module_create_info.pCode = reinterpret_cast<const GLuint *>(code.data());

	VkShaderModule shader_module;

	Debug::vkresult_check(
		vkCreateShaderModule(logical_device, &shader_module_create_info, nullptr, &shader_module),
	__FILE__, __LINE__, __FUNCTION__);

	return shader_module;
}