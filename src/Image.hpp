#pragma once

#include "MemDev.hpp"

struct GameObj {
	unsigned long long id {};
	float x {};
	float y {};
	float z {};
	float width {};
	float height {};
	float depth {};
	bool movable {false};
};

class Image {
public:

  static pair<vector<ImageTriple> &, vector<GameObj> &> create_game_obj(
    const VkDevice &logical_device,
    const VkQueue &graphics_queue,
    const char *image_path,
    const char *obj_path,
    const size_t obj_count,
		const float width,
		const float height,
		const float depth,
		const bool movable);

	static ImageTriple create_image(
		const VkDevice &logical_device,
		const GLuint width,
		const GLuint height,
		const VkFormat format,
		const VkImageTiling tiling,
		const VkImageUsageFlags usage);

	static VkImageView create_image_view(
		const VkDevice &logical_device,
		const VkImage image,
		const VkFormat format,
		const VkImageAspectFlags aspect_flags);

	static ImageTriple create_depthimage(const VkDevice &logical_device, const GLuint width, const GLuint height);
	static VkSampler & create_texsampler(const VkPhysicalDevice &physical_device, const VkDevice &logical_device);

	static VkDescriptorPool & create_descriptor_pool(const VkDevice &logical_device, const GLuint swapchain_image_count);
	static vector<VkDescriptorSetLayout> & create_descriptor_set_layouts(const VkDevice &logical_device);
	static pair<vector<VkBuffer> &, vector<VkDeviceMemory> &> create_uniform_buffers(
		const VkDevice &logical_device,
		const GLuint swapchain_image_count,
		const size_t obj_count,
    const VkDeviceSize ubo_size_aligned);
	static vector<VkDescriptorSet> & create_descriptor_sets(
		const VkDevice &logical_device,
		const GLuint swapchain_image_count,
    const VkDeviceSize ubo_size_aligned);

private:

	static void create_teximage(
		const VkDevice &logical_device,
    const VkQueue &graphics_queue,
		const char *image_path);

	static void copy_buffer_to_image(
		const VkBuffer &buffer,
		const VkImage &image,
		const GLuint width,
		const GLuint height,
		const VkQueue &graphics_queue);

	static void transition_image_layout(
		const VkImage &image,
		const VkImageLayout old_layout,
		const VkImageLayout new_layout,
		const VkQueue &graphics_queue);

};