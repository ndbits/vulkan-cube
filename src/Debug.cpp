#include "Debug.hpp"

#include "gtx/string_cast.hpp"
#include <cctype>
#include <typeinfo>

static VkDebugUtilsMessengerEXT debug_messenger;

void Debug::check_validation_layer_support() {
	uint32_t layer_count;
	vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

	vector<VkLayerProperties> available_layers(layer_count);
	vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

	for (const char *layer_name : validation_layers) {
		bool layer_found = false;

		for (const auto &layer_properties : available_layers) {
			if (strcmp(layer_name, layer_properties.layerName) == 0) {
				layer_found = true;
				break;
			}
		}

		if ( !layer_found) {
			cout << layer_name;
			throw std::runtime_error(" layer not supported or not loaded!\nUse: source vulkansdk/setup-env.sh");
		}
	}
}

VkDebugUtilsMessengerEXT & Debug::create_messenger(VkInstance &instance) {

	if ( !enable_validation_layers) {
		throw std::runtime_error("Validation layers not enabled!");
	};

	VkDebugUtilsMessengerCreateInfoEXT create_info_ext;
		create_info_ext = {};
		create_info_ext.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		create_info_ext.messageSeverity =
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		create_info_ext.messageType =
			VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		create_info_ext.pfnUserCallback = callback;

	auto debug_utils_func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (debug_utils_func != nullptr) {
        if(debug_utils_func(instance, &create_info_ext, nullptr, &debug_messenger) != VK_SUCCESS) {
        	throw std::runtime_error("failed to set up debug messenger!");
        }
    }
    return debug_messenger;
}

void Debug::destroy_messenger(VkInstance &instance, VkDebugUtilsMessengerEXT &messenger) {
	auto destroy_debug_utils_func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (destroy_debug_utils_func != nullptr) {
		destroy_debug_utils_func(instance, messenger, nullptr);
	}
}

VKAPI_ATTR VkBool32 VKAPI_CALL Debug::callback (VkDebugUtilsMessageSeverityFlagBitsEXT severity, VkDebugUtilsMessageTypeFlagsEXT type, const VkDebugUtilsMessengerCallbackDataEXT *callback_data, void *user_data) {
		(void) severity;
		(void) user_data;
		if (type == 2) { /* Validation Error */
			cerr << callback_data->pMessage << endl;
		}
		return VK_FALSE;
}

/* https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VkResult.html */
void Debug::vkresult_check(int vkresult, const char *file, int line, const char *function, bool success) {

	switch (vkresult) {
		case VK_SUCCESS:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": \U0001F44C\U0001F920" << endl;
			}
		break;
		case VK_NOT_READY:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_NOT_READY \U0001F98E" << endl;
			}
		break;
		case VK_TIMEOUT:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_TIMEOUT \U0001F47E" << endl;
			}
		break;
		case VK_EVENT_SET:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_EVENT_SET \U0001F47D" << endl;
			}
		break;
		case VK_EVENT_RESET:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_EVENT_RESET \U0001F4A9" << endl;
			}
		break;
		case VK_INCOMPLETE:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_INCOMPLETE \U0001F648" << endl;
			}
		break;
		case VK_SUBOPTIMAL_KHR:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_SUBOPTIMAL_KHR \U0001F926" << endl;
			}
		break;
		case VK_THREAD_IDLE_KHR:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_THREAD_IDLE_KHR \U0001F405" << endl;
			}
		break;
		case VK_THREAD_DONE_KHR:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_THREAD_DONE_KHR \U0001F98F" << endl;
			}
		break;
		case VK_OPERATION_DEFERRED_KHR:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_OPERATION_DEFERRED_KHR \U0001F987" << endl;
			}
		break;
		case VK_OPERATION_NOT_DEFERRED_KHR:
			if (success) {
				cout << file << "-" << std::to_string(line) << "-" << function << ": VK_OPERATION_NOT_DEFERRED_KHR \U0001F99C" << endl;
			}
		break;
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_OUT_OF_HOST_MEMORY \U0001F41E");
		break;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_OUT_OF_DEVICE_MEMORY \U0001F41E");
		break;
		case VK_ERROR_INITIALIZATION_FAILED:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_INITIALIZATION_FAILED \U0001F41E");
		break;
		case VK_ERROR_DEVICE_LOST:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_DEVICE_LOST \U0001F41E");
		break;
		case VK_ERROR_MEMORY_MAP_FAILED:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_MEMORY_MAP_FAILED \U0001F41E");
		break;
		case VK_ERROR_LAYER_NOT_PRESENT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_LAYER_NOT_PRESENT \U0001F41E");
		break;
		case VK_ERROR_EXTENSION_NOT_PRESENT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_EXTENSION_NOT_PRESENT \U0001F41E");
		break;
		case VK_ERROR_FEATURE_NOT_PRESENT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_FEATURE_NOT_PRESENT \U0001F41E");
		break;
		case VK_ERROR_INCOMPATIBLE_DRIVER:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_INCOMPATIBLE_DRIVER \U0001F41E");
		break;
		case VK_ERROR_TOO_MANY_OBJECTS:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_TOO_MANY_OBJECTS \U0001F41E");
		break;
		case VK_ERROR_FORMAT_NOT_SUPPORTED:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_FORMAT_NOT_SUPPORTED \U0001F41E");
		break;
		case VK_ERROR_FRAGMENTED_POOL:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_FRAGMENTED_POOL \U0001F41E");
		break;
		case VK_ERROR_UNKNOWN:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_UNKNOWN \U0001F41E");
		break;
		case VK_ERROR_OUT_OF_POOL_MEMORY:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_OUT_OF_POOL_MEMORY \U0001F41E");
		break;
		case VK_ERROR_INVALID_EXTERNAL_HANDLE:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_INVALID_EXTERNAL_HANDLE \U0001F41E");
		break;
		case VK_ERROR_FRAGMENTATION:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_FRAGMENTATION \U0001F41E");
		break;
		case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS \U0001F41E");
		break;
		case VK_PIPELINE_COMPILE_REQUIRED_EXT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_PIPELINE_COMPILE_REQUIRED_EXT \U0001F41E");
		break;
		case VK_INCOMPATIBLE_SHADER_BINARY_EXT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_INCOMPATIBLE_SHADER_BINARY_EXT \U0001F41E");
		break;
		case VK_ERROR_SURFACE_LOST_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_SURFACE_LOST_KHR \U0001F41E");
		break;
		case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_NATIVE_WINDOW_IN_USE_KHR \U0001F41E");
		break;
		case VK_ERROR_OUT_OF_DATE_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_OUT_OF_DATE_KHR \U0001F41E");
		break;
		case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_INCOMPATIBLE_DISPLAY_KHR \U0001F41E");
		break;
			#ifdef VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR
		case VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR \U0001F41E");
		break;
			#endif
			#ifdef VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KH
		case VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR \U0001F41E");
		break;
			#endif
			#ifdef VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR
		case VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR \U0001F41E");
		break;
			#endif
			#ifdef VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR
		case VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("K_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR \U0001F41E");
		break;
			#endif
			#ifdef VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR
		case VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR \U0001F41E");
		break;
			#endif
			#ifdef VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR
		case VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR \U0001F41E");
		break;
			#endif
		case VK_ERROR_NOT_PERMITTED_KHR:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_NOT_PERMITTED_(KHR/EXT) \U0001F41E");
		break;
		case VK_ERROR_VALIDATION_FAILED_EXT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_VALIDATION_FAILED_EXT \U0001F41E");
		break;
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_VALIDATION_FAILED_EXT \U0001F41E");
		break;
		case VK_ERROR_INVALID_SHADER_NV:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_INVALID_SHADER_NV \U0001F41E");
		break;
		case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT \U0001F41E");
		break;
		case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT:
			cout << file << "-" << std::to_string(line) << "-" << function << ": ";
			throw runtime_error("VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT \U0001F41E");
		break;
		default:
			throw runtime_error("VK_ERROR_UNKNOWN \U0001F41E");
		break;
	}
}


void Debug::flprint(VkDescriptorSet desc, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " VkDescriptorSet: " << desc << endl;
}
void Debug::flprint(VkDescriptorSetLayout desc_layout, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " VkDescriptorSetLayout: " << desc_layout << endl;
}
void Debug::flprint(VkBuffer vkb, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " VkBuffer: " << vkb << endl;
}
void Debug::flprint(VkDeviceMemory vkdm, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " VkDeviceMemory: " << vkdm << endl;
}
void Debug::flprint(VkImage vki, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " VkImage: " << vki << endl;
}
void Debug::flprint(VkImageView vkiv, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " VkImageView: " << vkiv << endl;
}
void Debug::flprint(const char *message, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " Cstr: " << message << endl;
}
void Debug::flprint(string message, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " str: " << message << endl;
}
void Debug::flprint(glm::vec2 obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " glm::" << glm::to_string(obj) << endl;
}
void Debug::flprint(glm::vec3 obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " glm::" << glm::to_string(obj) << endl;
}
void Debug::flprint(glm::mat4 obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " glm::" << glm::to_string(obj) << endl;
}
void Debug::flprint(GLfloat obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " GLfloat: " << std::fixed << std::setprecision(6) << obj << endl;
}
void Debug::flprint(int8_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " int8hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}
void Debug::flprint(uint8_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " uint8hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}
void Debug::flprint(int16_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " int16hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}
void Debug::flprint(uint16_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " uint16hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}
void Debug::flprint(int32_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " int32hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}
void Debug::flprint(uint32_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " uint32hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}
void Debug::flprint(int64_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " int64hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}
void Debug::flprint(uint64_t obj, const char *file, int line) {
	cout << file << "-" << std::to_string(line) << " uint64hex: " << std::hex << std::showbase << obj;
	cout << " dec: " << std::dec << obj << endl;
}