#pragma once

#include "Image.hpp"

typedef bool bool_array_349[349];

enum Direction {
	DOWN,
	LEFT,
	UP,
	RIGHT,
	BACK,
	FRONT
};

class Controller {
public:

	static void rotate_object(
		const VkDevice &logical_device,
		const VkExtent2D &swapchain_extent,
		const GLuint current_image,
		vector<VkDeviceMemory> &swapchain_uniform_buffers_memory,
		pair<void *, VkDeviceSize> ubuffer_n_ubo_size_aligned,
		vector<GameObj> &game_objs);

	static bool_array_349 & pressed_keys_array(void);
	static void key_press_callback(GLFWwindow *window, const int key, const int scancode, const int action, const int mods);
	static void key_action(const int key, GLFWwindow *window);
	static void mouse_press_callback(GLFWwindow* window, const int button, const int action, const int mods);
	static void mouse_scroll_callback(GLFWwindow *window, const double scroll_x, const double scroll_y);
	static void mouse_position_callback(GLFWwindow *window, const double pos_x, const double pos_y);

private:

	static void collision_check(GameObj &obj1, GameObj &obj2);
	static Direction direction_check(const vec3 distance_vec);

};