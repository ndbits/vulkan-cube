#include "Music.hpp"

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <stdint.h>
#include <unistd.h>
#include "portaudio.h"
#include "safe_mem_lib.h"
#include "safe_str_lib.h"

using std::cout;
using std::endl;
using std::cerr;

#define CHECK(x) { if( !(x)) { \
	cerr << "Failure at: " << __FILE__ <<":" << __LINE__<<":" << #x << endl;\
}}

PaStream *stream;
FILE *wavfile;
PaSampleFormat sampleFormat;
int numChannels, bytesPerSample, sampleRate, bitsPerSample;

template<typename T>
T fread_num(FILE *f) {
	T value;
	CHECK(fread(&value, sizeof(value), 1, f) == 1);
	return value;
}

int stream_callback(const void *input, void *output, unsigned long frameCount, const PaStreamCallbackTimeInfo *timeInfo, PaStreamCallbackFlags statusFlags, void *userData) {

	(void) input;
	(void) timeInfo;
	(void) statusFlags;
	(void) userData;

	size_t numRead = fread(output, bytesPerSample * numChannels, frameCount, wavfile);
	output = (uint8_t*)output + numRead * numChannels * bytesPerSample;
	frameCount -= numRead;
	
	if(frameCount > 0) {
		/* Using safe memset_s() */
		memset_s(output, frameCount * numChannels * bytesPerSample, 0, frameCount * numChannels * bytesPerSample);
		return paComplete;
	}
	
	return paContinue;
}

bool portaudio_open() {
	CHECK(Pa_Initialize() == paNoError);

	PaStreamParameters outputParameters;

	outputParameters.device = Pa_GetDefaultOutputDevice();
	if (outputParameters.device == paNoDevice) {
		cerr << "Error: No audio device found! " << endl;
		return false;
	}

	outputParameters.channelCount = numChannels;
	outputParameters.sampleFormat = sampleFormat;
	outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultHighOutputLatency;
	outputParameters.hostApiSpecificStreamInfo = NULL;

	PaError ret = Pa_OpenStream(&stream, NULL, &outputParameters, sampleRate, paFramesPerBufferUnspecified, paClipOff, &stream_callback, NULL);

	if(ret != paNoError) {
		cerr << "Pa_OpenStream failed: (err "<< ret <<") "<< Pa_GetErrorText(ret) << endl;
		if(stream)
			Pa_CloseStream(stream);
		return false;
	}

	CHECK(Pa_StartStream(stream) == paNoError);
	return true;
}

std::string fread_str(FILE *f, size_t len) {
	std::string s(len, '\0');
	CHECK(fread(&s[0], 1, len, f) == len);
	return s;
}

void read_chunk(uint32_t chunkLen) {
	CHECK(chunkLen >= 16);
	uint16_t fmttag = fread_num<uint16_t>(wavfile); // 1: PCM (int). 3: IEEE float
	CHECK(fmttag == 1 || fmttag == 3);
	numChannels = fread_num<uint16_t>(wavfile);
	CHECK(numChannels > 0);
	printf_s("%i channels\n", numChannels);
	sampleRate = fread_num<uint32_t>(wavfile);
	printf_s("%i Hz\n", sampleRate);
	int byteRate = fread_num<uint32_t>(wavfile);
	uint16_t blockAlign = fread_num<uint16_t>(wavfile);
	bitsPerSample = fread_num<uint16_t>(wavfile);
	bytesPerSample = bitsPerSample / 8;
	CHECK(byteRate == sampleRate * numChannels * bytesPerSample);
	CHECK(blockAlign == numChannels * bytesPerSample);
	if(fmttag == 1 /*PCM*/) {
		switch(bitsPerSample) {
			case 8: sampleFormat = paInt8; break;
			case 16: sampleFormat = paInt16; break;
			case 32: sampleFormat = paInt32; break;
			default: CHECK(false);
		}
		printf_s("PCM %ibit int\n", bitsPerSample);
	} else {
		CHECK(fmttag == 3 /* IEEE float */);
		CHECK(bitsPerSample == 32);
		sampleFormat = paFloat32;
		printf_s("32bit float\n");
	}
	if(chunkLen > 16) {
		uint16_t extendedSize = fread_num<uint16_t>(wavfile);
		CHECK(chunkLen == static_cast<uint16_t>(18 + extendedSize));
		fseek(wavfile, extendedSize, SEEK_CUR);
	}
}

void Music::play() {
	char file_path[] = "music/Vim_-_ricky_babes_on_the_town.wav";

	wavfile = fopen(file_path, "r");
	CHECK(wavfile != NULL);

	CHECK(fread_str(wavfile, 4) == "RIFF");
	fread_num<uint32_t>(wavfile);
	CHECK(fread_str(wavfile, 4) == "WAVE");
	while(true) {
		std::string chunkName = fread_str(wavfile, 4);
		uint32_t chunkLen = fread_num<uint32_t>(wavfile);
		if(chunkName == "fmt ")
			read_chunk(chunkLen);
		else if(chunkName == "data") {
			CHECK(sampleRate != 0);
			CHECK(numChannels > 0);
			CHECK(bytesPerSample > 0);
			printf_s("len: %.0f secs\n", double(chunkLen / sampleRate / numChannels / bytesPerSample));
			break; // start playing now
		} else {
			// skip chunk
			CHECK(fseek(wavfile, chunkLen, SEEK_CUR) == 0);
		}
	}

	printf_s("start playing...");
	CHECK(portaudio_open());

	// wait until stream has finished playing
	while(Pa_IsStreamActive(stream) > 0)
		usleep(1000);

	printf_s("finished\n");
	fclose(wavfile);
	Pa_CloseStream(stream);
	Pa_Terminate();
}
