#include "Image.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

static vector<ImageTriple> teximage_triples {};
static vector<GameObj> game_objs {};
VkSampler texsampler {};

static vector<VkBuffer> swapchain_uniform_buffers {};
static vector<VkDeviceMemory> swapchain_uniform_buffers_memory {};

static VkDescriptorPool descriptor_pool {};
static vector<VkDescriptorSetLayout> descriptor_layouts {};
static vector<VkDescriptorSet> descriptor_sets {};

pair<vector<ImageTriple> &, vector<GameObj> &> Image::create_game_obj(
	const VkDevice &logical_device,
  const VkQueue &graphics_queue,
	const char *image_path,
  const char *obj_path,
  const size_t obj_count,
	const float width,
	const float height,
	const float depth,
	const bool movable) {

		game_objs.resize(obj_count);

    for (size_t i = 0; i < obj_count; i += 1) {
      create_teximage(logical_device, graphics_queue, image_path);
      MemDev::load_obj(obj_path);

			game_objs[i].id = i;
			game_objs[i].movable = movable;
			game_objs[i].width = width;
			game_objs[i].height = height;
			game_objs[i].depth  = depth;
			game_objs[i].x = i *  2.0f;
			game_objs[i].y = i *  2.0f;
			game_objs[i].z = i * -2.0f;
    }

    return make_pair(reference_wrapper(teximage_triples), reference_wrapper(game_objs));
  }

void Image::create_teximage(
	const VkDevice &logical_device,
  const VkQueue &graphics_queue,
	const char *image_path) {

	int img_width, img_height, img_channels;
	stbi_uc *img_file = stbi_load(image_path, &img_width, &img_height, &img_channels, STBI_rgb_alpha);
	if ( !img_file) {
		throw std::runtime_error("failed to load image!");
	}
	VkDeviceSize buffer_size = img_width * img_height * 4; /* 4 = rgba */

	VkDeviceMemory staging_buffer_memory;
	VkBuffer staging_buffer;
	MemDev::create_buffer(
			logical_device,
			buffer_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			staging_buffer,
			staging_buffer_memory);

	void *data;
	vkMapMemory(logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
		memcpy_s(data, static_cast<size_t>(buffer_size), img_file, static_cast<size_t>(buffer_size));
	vkUnmapMemory(logical_device, staging_buffer_memory);

	stbi_image_free(img_file);

	ImageTriple image_triple {create_image(
			logical_device,
			img_width,
			img_height,
			VK_FORMAT_B8G8R8A8_SRGB,
			VK_IMAGE_TILING_OPTIMAL,
			VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT)};

	transition_image_layout(image_triple.image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, graphics_queue);

	copy_buffer_to_image(staging_buffer, image_triple.image, img_width, img_height, graphics_queue);

	transition_image_layout(image_triple.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, graphics_queue);

	vkDestroyBuffer(logical_device, staging_buffer, nullptr);
	vkFreeMemory(logical_device, staging_buffer_memory, nullptr);

	image_triple.view = create_image_view(logical_device, image_triple.image, VK_FORMAT_B8G8R8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT);

	teximage_triples.push_back(image_triple);
}

ImageTriple Image::create_depthimage(const VkDevice &logical_device, const GLuint width, const GLuint height) {
	VkFormat depth_format {MemDev::get_depth_stencil_format()};
	ImageTriple depthimg_tripple {create_image(
			logical_device,
			width,
			height,
			depth_format,
			VK_IMAGE_TILING_OPTIMAL,
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)};

	depthimg_tripple.view = create_image_view(logical_device, depthimg_tripple.image, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT);
	return depthimg_tripple;
}

ImageTriple Image::create_image(
	const VkDevice &logical_device,
	const GLuint width,
	const GLuint height,
	const VkFormat format,
	const VkImageTiling tiling,
	const VkImageUsageFlags usage) {

	VkImageCreateInfo image_info {};
		image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		image_info.imageType = VK_IMAGE_TYPE_2D;
		image_info.extent.width = width;
		image_info.extent.height = height;
		image_info.extent.depth = 1;
		image_info.mipLevels = 1;
		image_info.arrayLayers = 1;
		image_info.format = format;
		image_info.tiling = tiling;
		image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		image_info.usage = usage;
		image_info.samples = VK_SAMPLE_COUNT_1_BIT;
		image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VkImage image;
	Debug::vkresult_check(
		vkCreateImage(logical_device, &image_info, nullptr, &image),
	__FILE__, __LINE__, __FUNCTION__);

	VkImageMemoryRequirementsInfo2 image_mem_req_info {};
		image_mem_req_info.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2;
		image_mem_req_info.image = image;

	VkMemoryRequirements2 mem_req {};
		mem_req.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;

	vkGetImageMemoryRequirements2(logical_device, &image_mem_req_info, &mem_req);

	VkMemoryAllocateInfo alloc_info {};
		alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		alloc_info.allocationSize = mem_req.memoryRequirements.size;
		alloc_info.memoryTypeIndex = MemDev::get_memory_type(mem_req.memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	VkDeviceMemory image_memory;
	Debug::vkresult_check(
		vkAllocateMemory(logical_device, &alloc_info, nullptr, &image_memory),
	__FILE__, __LINE__, __FUNCTION__);

	VkBindImageMemoryInfo bind_info {};
		bind_info.sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
		bind_info.image = image;
		bind_info.memory = image_memory;
		bind_info.memoryOffset = 0;

	vkBindImageMemory2(logical_device, 1, &bind_info);

	ImageTriple image_tripple {image, VK_NULL_HANDLE, image_memory};
	return image_tripple;
}

void Image::copy_buffer_to_image(
	const VkBuffer &buffer,
	const VkImage &image,
	const GLuint width,
	const GLuint height,
	const VkQueue &graphics_queue) {

	VkCommandBuffer command_buffer {MemDev::create_command_buffer()};

	VkBufferImageCopy2 region {};
		region.sType = VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2;
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;
		region.imageOffset = {0, 0, 0};
		region.imageExtent = {width, height, 1};

	VkCopyBufferToImageInfo2 buffer_to_image_info {};
		buffer_to_image_info.sType = VK_STRUCTURE_TYPE_COPY_BUFFER_TO_IMAGE_INFO_2;
		buffer_to_image_info.srcBuffer = buffer;
		buffer_to_image_info.dstImage = image;
		buffer_to_image_info.dstImageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		buffer_to_image_info.regionCount = 1;
		buffer_to_image_info.pRegions = &region;

	vkCmdCopyBufferToImage2(command_buffer, &buffer_to_image_info);

	MemDev::end_command_buffer(graphics_queue);
}

void Image::transition_image_layout(
	const VkImage &image,
	const VkImageLayout old_layout,
	const VkImageLayout new_layout,
	const VkQueue &graphics_queue) {

	VkCommandBuffer command_buffer {MemDev::create_command_buffer()};

	VkImageMemoryBarrier2 image_mem_barrier {};
		image_mem_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
		image_mem_barrier.oldLayout = old_layout;
		image_mem_barrier.newLayout = new_layout;
		image_mem_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		image_mem_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		image_mem_barrier.image = image;
		image_mem_barrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

	if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED && new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		image_mem_barrier.srcAccessMask = VK_ACCESS_2_NONE;
		image_mem_barrier.dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
		image_mem_barrier.srcStageMask = VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT;
		image_mem_barrier.dstStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
	} else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		image_mem_barrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
		image_mem_barrier.dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT;
		image_mem_barrier.srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
		image_mem_barrier.dstStageMask = VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT;
	} else {
		throw invalid_argument("unsupported layout transition!");
	}

	VkDependencyInfo dependency_info {};
	dependency_info.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	dependency_info.imageMemoryBarrierCount = 1;
	dependency_info.pImageMemoryBarriers = &image_mem_barrier;

	vkCmdPipelineBarrier2(command_buffer, &dependency_info);

	MemDev::end_command_buffer(graphics_queue);
}

VkImageView Image::create_image_view(
	const VkDevice &logical_device,
	const VkImage image,
	const VkFormat format,
	const VkImageAspectFlags aspect_flags) {

	VkImageViewCreateInfo view_info {};
		view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		view_info.image = image;
		view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
		view_info.format = format;
		view_info.components = {VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY,
			VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY}; /* rgba */
		view_info.subresourceRange.aspectMask = aspect_flags;
		view_info.subresourceRange.baseMipLevel = 0;
		view_info.subresourceRange.levelCount = 1;/* mip_levels */
		view_info.subresourceRange.baseArrayLayer = 0;
		view_info.subresourceRange.layerCount = 1;

	VkImageView image_view;
	Debug::vkresult_check(
		vkCreateImageView(logical_device, &view_info, nullptr, &image_view),
	__FILE__, __LINE__, __FUNCTION__);

	return image_view;
}

VkSampler & Image::create_texsampler(const VkPhysicalDevice &physical_device, const VkDevice &logical_device) {
	VkPhysicalDeviceProperties properties {};
	vkGetPhysicalDeviceProperties(physical_device, &properties);

	VkSamplerCreateInfo sampler_info {};
		sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		sampler_info.magFilter = VK_FILTER_LINEAR;
		sampler_info.minFilter = VK_FILTER_LINEAR;
		sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler_info.anisotropyEnable = VK_TRUE;
		sampler_info.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
		sampler_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		sampler_info.unnormalizedCoordinates = VK_FALSE;
		sampler_info.compareEnable = VK_FALSE;
		sampler_info.compareOp = VK_COMPARE_OP_ALWAYS;
		sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

	Debug::vkresult_check(
		vkCreateSampler(logical_device, &sampler_info, nullptr, &texsampler),
	__FILE__, __LINE__, __FUNCTION__);

	return texsampler;
}

VkDescriptorPool & Image::create_descriptor_pool(const VkDevice &logical_device, const GLuint swapchain_image_count) {
	array<VkDescriptorPoolSize, 3> pool_sizes {};

	pool_sizes[0].type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
	/* swapchain_image_count * textures[] in fragment shader */
	pool_sizes[0].descriptorCount = static_cast<GLuint>(swapchain_image_count);
	pool_sizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	pool_sizes[1].descriptorCount = static_cast<GLuint>(swapchain_image_count);
	pool_sizes[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
	pool_sizes[2].descriptorCount = swapchain_image_count;

	VkDescriptorPoolCreateInfo pool_info {};
	pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	pool_info.poolSizeCount = static_cast<GLuint>(pool_sizes.size());
	pool_info.pPoolSizes = pool_sizes.data();
	pool_info.maxSets = static_cast<GLuint>(swapchain_image_count);

	Debug::vkresult_check(
		vkCreateDescriptorPool(logical_device, &pool_info, nullptr, &descriptor_pool),
	__FILE__, __LINE__, __FUNCTION__);

	return descriptor_pool;
}

vector<VkDescriptorSetLayout> & Image::create_descriptor_set_layouts(const VkDevice &logical_device) {

	VkDescriptorSetLayoutBinding texture_layout_binding {};
		texture_layout_binding.binding = 0;
		/* descriptorCount = textures[] in fragment shader */
		texture_layout_binding.descriptorCount = 1;
		texture_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		texture_layout_binding.pImmutableSamplers = nullptr;
		texture_layout_binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding sampler_layout_binding {};
		sampler_layout_binding.binding = 1;
		sampler_layout_binding.descriptorCount = 1;
		sampler_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		sampler_layout_binding.pImmutableSamplers = nullptr;
		sampler_layout_binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding ubo_layout_binding {};
		ubo_layout_binding.binding = 2;
		ubo_layout_binding.descriptorCount = 1;
		ubo_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		ubo_layout_binding.pImmutableSamplers = nullptr;
		ubo_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	array<VkDescriptorSetLayoutBinding, 3> bindings = {texture_layout_binding, sampler_layout_binding, ubo_layout_binding};
	VkDescriptorSetLayoutCreateInfo layout_info {};
		layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layout_info.bindingCount = static_cast<GLuint>(bindings.size());
		layout_info.pBindings = bindings.data();

	VkDescriptorSetLayout descriptor_set_layout;
	Debug::vkresult_check(
		vkCreateDescriptorSetLayout(logical_device, &layout_info, nullptr, &descriptor_set_layout),
	__FILE__, __LINE__, __FUNCTION__);

	descriptor_layouts.push_back(descriptor_set_layout);
	return descriptor_layouts;
}

pair<vector<VkBuffer> &, vector<VkDeviceMemory> &> Image::create_uniform_buffers(
	const VkDevice &logical_device,
	const GLuint swapchain_image_count,
	const size_t obj_count,
  const VkDeviceSize ubo_size_aligned) {

	swapchain_uniform_buffers.resize(swapchain_image_count);
	swapchain_uniform_buffers_memory.resize(swapchain_image_count);

	VkDeviceSize buffer_size = obj_count * ubo_size_aligned;

	for (size_t i = 0; i < swapchain_image_count; i += 1) {
		MemDev::create_buffer(
			logical_device,
			buffer_size,
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			swapchain_uniform_buffers[i], swapchain_uniform_buffers_memory[i]);
	}

	return make_pair(reference_wrapper(swapchain_uniform_buffers), reference_wrapper(swapchain_uniform_buffers_memory));
}

vector<VkDescriptorSet> & Image::create_descriptor_sets(
	const VkDevice &logical_device,
	const GLuint swapchain_image_count,
  const VkDeviceSize ubo_size_aligned) {

	descriptor_layouts.resize(swapchain_image_count, descriptor_layouts[0]);
	VkDescriptorSetAllocateInfo alloc_info {};
		alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		alloc_info.descriptorPool = descriptor_pool;
		alloc_info.descriptorSetCount = swapchain_image_count;
		alloc_info.pSetLayouts = descriptor_layouts.data();

	descriptor_sets.resize(swapchain_image_count);

	Debug::vkresult_check(
		vkAllocateDescriptorSets(logical_device, &alloc_info, descriptor_sets.data()),
	__FILE__, __LINE__, __FUNCTION__);

	array<VkWriteDescriptorSet, 3> descriptor_writes {};

	VkDescriptorImageInfo image_info {};

	VkDescriptorBufferInfo buffer_info {};

	for (size_t i = 0; i < swapchain_image_count; i += 1) {

		image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		image_info.imageView = teximage_triples[0].view;
		image_info.sampler = texsampler;

		descriptor_writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptor_writes[0].dstSet = descriptor_sets[i];
		descriptor_writes[0].dstBinding = 0;
		descriptor_writes[0].dstArrayElement = 0;
		descriptor_writes[0].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		descriptor_writes[0].descriptorCount = 1;
		descriptor_writes[0].pBufferInfo = nullptr;
		descriptor_writes[0].pImageInfo = &image_info;

		descriptor_writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptor_writes[1].dstSet = descriptor_sets[i];
		descriptor_writes[1].dstBinding = 1;
		descriptor_writes[1].dstArrayElement = 0;
		descriptor_writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		descriptor_writes[1].descriptorCount = 1;
		descriptor_writes[1].pBufferInfo = nullptr;
		descriptor_writes[1].pImageInfo = &image_info;

		buffer_info.buffer = swapchain_uniform_buffers[i];
		// Start at the beginning of the buffer
		buffer_info.offset = 0;
		// For each offset ubo_size_aligned is the size used for that draw call
		buffer_info.range = ubo_size_aligned;

		descriptor_writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptor_writes[2].dstSet = descriptor_sets[i];
		descriptor_writes[2].dstBinding = 2;
		descriptor_writes[2].dstArrayElement = 0;
		descriptor_writes[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		descriptor_writes[2].descriptorCount = 1;
		descriptor_writes[2].pBufferInfo = &buffer_info;
		descriptor_writes[2].pImageInfo = nullptr;

		vkUpdateDescriptorSets(logical_device, static_cast<GLuint>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);

	}

	return descriptor_sets;
}