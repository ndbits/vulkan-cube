#pragma once

#include "Core.hpp"

const vector<const char *> validation_layers {
	"VK_LAYER_KHRONOS_validation"
};

const VkValidationFeatureEnableEXT validation_features[] {
	VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
	VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT
};

const VkValidationFeatureDisableEXT disabled_validation_features[] {
	// VK_VALIDATION_FEATURE_DISABLE_ALL_EXT
	// VK_VALIDATION_FEATURE_DISABLE_SHADERS_EXT,
	// VK_VALIDATION_FEATURE_DISABLE_THREAD_SAFETY_EXT,
	// VK_VALIDATION_FEATURE_DISABLE_API_PARAMETERS_EXT,
	// VK_VALIDATION_FEATURE_DISABLE_OBJECT_LIFETIMES_EXT,
	// VK_VALIDATION_FEATURE_DISABLE_CORE_CHECKS_EXT,
	// VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT,
	// VK_VALIDATION_FEATURE_DISABLE_SHADER_VALIDATION_CACHE_EXT
};

const bool enable_validation_layers = true;

class Debug {
public:

	static void check_validation_layer_support(void);

	static VkDebugUtilsMessengerEXT & create_messenger(VkInstance &instance);
	static void destroy_messenger(VkInstance &instance, VkDebugUtilsMessengerEXT &messenger);

	/* Throws only if error with type and location, excluding non-error codes.
		Debug::vkresult_check(vkCreateDevice(...), __FILE__, __LINE__, __FUNCTION__);
	If true added it prints message on success.
		Debug::vkresult_check(vkCreateDevice(...), __FILE__, __LINE__, __FUNCTION__, true);
	*/
	static void vkresult_check(int vkresult, const char *file, int line, const char *function, bool success = false);

	/* Dumps info about a variable with location.
		Debug::flprint("str", __FILE__, __LINE__);
		Debug::flprint(var, __FILE__, __LINE__);
	*/
	static void flprint(VkDescriptorSet desc, const char *file, int line);
	static void flprint(VkDescriptorSetLayout desc_layout, const char *file, int line);
	static void flprint(VkBuffer vkb, const char *file, int line);
	static void flprint(VkDeviceMemory vkdm, const char *file, int line);
	static void flprint(VkImage vki, const char *file, int line);
	static void flprint(VkImageView vkiv, const char *file, int line);
	static void flprint(const char *message, const char *file, int line);
	static void flprint(string message, const char *file, int line);
	static void flprint(glm::vec2 obj, const char *file, int line);
	static void flprint(glm::vec3 obj, const char *file, int line);
	static void flprint(glm::mat4 obj, const char *file, int line);
	static void flprint(GLfloat obj, const char *file, int line);
	static void flprint(int8_t obj, const char *file, int line);
	static void flprint(uint8_t obj, const char *file, int line);
	static void flprint(int16_t obj, const char *file, int line);
	static void flprint(uint16_t obj, const char *file, int line);
	static void flprint(int32_t obj, const char *file, int line);
	static void flprint(uint32_t obj, const char *file, int line);
	static void flprint(int64_t obj, const char *file, int line);
	static void flprint(uint64_t obj, const char *file, int line);

private:

	static VKAPI_ATTR VkBool32 VKAPI_CALL callback (VkDebugUtilsMessageSeverityFlagBitsEXT severity, VkDebugUtilsMessageTypeFlagsEXT type, const VkDebugUtilsMessengerCallbackDataEXT *callback_data, void *user_data);
};