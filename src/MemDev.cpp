#include "MemDev.hpp"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tinyobjloader/tiny_obj_loader.h"

static VkPhysicalDevice physical_device {};
static VkDevice logical_device {};

static pair<VkCommandBuffer, VkCommandPool> command_buff_n_pool {};

static QueueFamilyIndices queue_indices {};

static vector< pair<VkBuffer, VkDeviceMemory> > vertex_buffer_n_mem {};
static vector< pair<VkBuffer, VkDeviceMemory> > index_buffer_n_mem {};
static vector<VkCommandBuffer> swapchain_command_buffers {};

static vector< pair< vector<Vertex>, vector<GLushort> > > vertices_indices {};

const vector<const char *> device_extensions {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	#ifdef VK_KHR_PORTABILITY_SUBSET_EXTENSION_NAME
		VK_KHR_PORTABILITY_SUBSET_EXTENSION_NAME
	#endif
};

VkPhysicalDevice & MemDev::pick_physical_device(const VkInstance &instance) {
	GLuint device_count = 0;
	vkEnumeratePhysicalDevices(instance, &device_count, nullptr);

	if (device_count == 0) {
		throw runtime_error("failed to find GPUs with Vulkan support!");
	}

	vector<VkPhysicalDevice> devices(device_count);
	vkEnumeratePhysicalDevices(instance, &device_count, devices.data());

	for (auto &dev : devices) {
		physical_device = dev;
		break;
	}

	if (physical_device == VK_NULL_HANDLE) {
		throw runtime_error("failed to find a suitable GPU!");
	}
	return physical_device;
}

GLuint MemDev::get_device_mask(const VkInstance &instance) {

	GLuint device_group_count {};
	vkEnumeratePhysicalDeviceGroups(instance, &device_group_count, nullptr);
	vector<VkPhysicalDeviceGroupProperties> device_groups(device_group_count);
	for (size_t i = 0; i < device_group_count; i += 1) {
		device_groups[i].sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES;
	}
	vkEnumeratePhysicalDeviceGroups(instance, &device_group_count, device_groups.data());
	// mask for all devices in the first physical device group
	GLuint device_mask = (1 << device_groups[0].physicalDeviceCount) - 1;

	return device_mask;
}

VkDevice & MemDev::create_logical_device(const VkSurfaceKHR &surface) {
	QueueFamilyIndices queue_indices {MemDev::get_queue_families(surface)};

	GLfloat queue_priority = 1.0f;
	vector<VkDeviceQueueCreateInfo> queue_infos {};
	std::set<GLuint> queue_families = {queue_indices.graphicsFamily.value(), queue_indices.presentFamily.value()};
	for (GLuint family : queue_families) {
		VkDeviceQueueCreateInfo queue_info {};
		queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queue_info.queueFamilyIndex = family;
		queue_info.queueCount = 1;
		queue_info.pQueuePriorities = &queue_priority;
		queue_infos.push_back(queue_info);
	}

	// VK_KHR_synchronization2 extension
	VkPhysicalDeviceSynchronization2Features synchronization_features {};
	synchronization_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES;
	synchronization_features.synchronization2 = VK_TRUE;

	// VK_KHR_dynamic_rendering extension
	VkPhysicalDeviceDynamicRenderingFeatures dynamic_rendering_features {};
    dynamic_rendering_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES;
		dynamic_rendering_features.pNext = &synchronization_features;
    dynamic_rendering_features.dynamicRendering = VK_TRUE;

	VkPhysicalDeviceFeatures device_features {};
	device_features.samplerAnisotropy = VK_TRUE;

	VkDeviceCreateInfo device_create_info {};
	device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	device_create_info.pNext = &dynamic_rendering_features;
	device_create_info.queueCreateInfoCount = static_cast<GLuint>(queue_infos.size());
	device_create_info.pQueueCreateInfos = queue_infos.data();
	device_create_info.pEnabledFeatures = &device_features;
	device_create_info.enabledExtensionCount = static_cast<GLuint>(device_extensions.size());
	device_create_info.ppEnabledExtensionNames = device_extensions.data();

	Debug::vkresult_check(
		vkCreateDevice(physical_device, &device_create_info, nullptr, &logical_device),
	__FILE__, __LINE__, __FUNCTION__);

	return logical_device;
}

VkCommandPool & MemDev::create_command_pool(const VkSurfaceKHR &surface) {
	queue_indices = get_queue_families(surface);

	VkCommandPoolCreateInfo pool_info {};
		pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		pool_info.queueFamilyIndex = queue_indices.graphicsFamily.value();

	Debug::vkresult_check(
		vkCreateCommandPool(logical_device, &pool_info, nullptr, &command_buff_n_pool.second),
	__FILE__, __LINE__, __FUNCTION__);

	return command_buff_n_pool.second;
}

VkCommandBuffer & MemDev::create_command_buffer() {
	VkCommandBufferAllocateInfo alloc_info {};
		alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		alloc_info.commandPool = command_buff_n_pool.second;
		alloc_info.commandBufferCount = 1;

	vkAllocateCommandBuffers(logical_device, &alloc_info, &command_buff_n_pool.first);

	VkCommandBufferBeginInfo begin_info {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(command_buff_n_pool.first, &begin_info);

	return command_buff_n_pool.first;
}

void MemDev::end_command_buffer(const VkQueue &graphics_queue) {

	vkEndCommandBuffer(command_buff_n_pool.first);

	VkCommandBufferSubmitInfo command_buff_submit_info {};
		command_buff_submit_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
		command_buff_submit_info.commandBuffer = command_buff_n_pool.first;

	VkSubmitInfo2 submit_info {};
		submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
		submit_info.commandBufferInfoCount = 1;
		submit_info.pCommandBufferInfos = &command_buff_submit_info;

	vkQueueSubmit2(graphics_queue, 1, &submit_info, VK_NULL_HANDLE);
	vkQueueWaitIdle(graphics_queue);

	vkFreeCommandBuffers(logical_device, command_buff_n_pool.second, 1, &command_buff_n_pool.first);
}

vector<VkCommandBuffer> & MemDev::create_swapchain_command_buffers(
	const vector<VkDescriptorSet> &descriptor_sets,
	const VkPipeline &graphics_pipeline,
	const VkPipelineLayout &graphics_pipeline_layout,
	const VkExtent2D &swapchain_extent,
	const GLuint swapchain_image_count,
  const VkDeviceSize ubo_size_aligned,
	tuple<VkSwapchainKHR, vector<VkImage> &, vector<VkImageView> &, ImageTriple &> swapchain_imgs_views_depthtriple) {

	swapchain_command_buffers.resize(swapchain_image_count);

	VkCommandBufferAllocateInfo alloc_info {};
	alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	alloc_info.commandPool = command_buff_n_pool.second;
	alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	alloc_info.commandBufferCount = swapchain_image_count;

	Debug::vkresult_check(
		vkAllocateCommandBuffers(logical_device, &alloc_info, swapchain_command_buffers.data()),
	__FILE__, __LINE__, __FUNCTION__);

	const size_t ver_ind_size {vertices_indices.size()};

	for (size_t i = 0; i < swapchain_image_count; i += 1) {
		VkCommandBufferBeginInfo begin_info {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		Debug::vkresult_check(
			vkBeginCommandBuffer(swapchain_command_buffers[i], &begin_info),
		__FILE__, __LINE__, __FUNCTION__);

		create_image_memory_barrier(
			swapchain_command_buffers[i],
			get<1>(swapchain_imgs_views_depthtriple)[i],
			VK_ACCESS_2_NONE,
			VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
			VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
			VkImageSubresourceRange { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 });

		create_image_memory_barrier(
			swapchain_command_buffers[i],
			get<3>(swapchain_imgs_views_depthtriple).image,
			VK_ACCESS_2_NONE,
			VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
			VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
			VkImageSubresourceRange { VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, 0, 1, 0, 1 });

		VkRenderingAttachmentInfo color_attachment {};
			color_attachment.sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO;
			color_attachment.imageView = get<2>(swapchain_imgs_views_depthtriple)[i];
			color_attachment.imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			color_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			color_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			color_attachment.clearValue.color = {{ 0.0f, 0.0f, 0.0f, 0.0f }};

		VkRenderingAttachmentInfo depth_stencil_attachment {};
			depth_stencil_attachment.sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO;
			depth_stencil_attachment.imageView = get<3>(swapchain_imgs_views_depthtriple).view;
			depth_stencil_attachment.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			depth_stencil_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depth_stencil_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			depth_stencil_attachment.clearValue.depthStencil = { 1.0f, 0 };

		VkRenderingInfo rendering_info {};
			rendering_info.sType = VK_STRUCTURE_TYPE_RENDERING_INFO;
			rendering_info.renderArea = { {0, 0}, swapchain_extent };
			rendering_info.layerCount = 1;
			rendering_info.colorAttachmentCount = 1;
			rendering_info.pColorAttachments = &color_attachment;
			rendering_info.pDepthAttachment = &depth_stencil_attachment;
			rendering_info.pStencilAttachment = &depth_stencil_attachment;

		vkCmdBeginRendering(swapchain_command_buffers[i], &rendering_info);

			VkDeviceSize offsets[] = {0};
			vkCmdBindPipeline(swapchain_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline);

			/* update data for each texture */
			for (size_t vi = 0; vi < ver_ind_size; vi += 1) {
				const uint32_t dynamic_offset = vi * ubo_size_aligned;

				VkBuffer vertex_buffers[] = {vertex_buffer_n_mem[vi].first};
				vkCmdBindVertexBuffers(swapchain_command_buffers[i], 0, 1, vertex_buffers, offsets);
				vkCmdBindIndexBuffer(swapchain_command_buffers[i], index_buffer_n_mem[vi].first, 0, VK_INDEX_TYPE_UINT16);
				vkCmdBindDescriptorSets(swapchain_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline_layout, 0, 1, &descriptor_sets[i], 1, &dynamic_offset);
				vkCmdDrawIndexed(swapchain_command_buffers[i], static_cast<GLuint>(vertices_indices[vi].second.size()), 1, 0, 0, 0);
			}

		VkSubpassEndInfo subpass_end_info {};
			subpass_end_info.sType = VK_STRUCTURE_TYPE_SUBPASS_END_INFO;

		vkCmdEndRendering(swapchain_command_buffers[i]);

		Debug::vkresult_check(
			vkEndCommandBuffer(swapchain_command_buffers[i]),
		__FILE__, __LINE__, __FUNCTION__);
	}
	return swapchain_command_buffers;
}

void MemDev::create_buffer(
	const VkDevice &logical_device,
	const VkDeviceSize size,
	const VkBufferUsageFlags usage,
	const VkMemoryPropertyFlags properties,
	VkBuffer &buffer,
	VkDeviceMemory &buffer_memory) {

	VkBufferCreateInfo buffer_info {};
		buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buffer_info.size = size;
		buffer_info.usage = usage;
		buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	Debug::vkresult_check(
		vkCreateBuffer(logical_device, &buffer_info, nullptr, &buffer),
	__FILE__, __LINE__, __FUNCTION__);

	VkBufferMemoryRequirementsInfo2 buff_mem_req_info {};
		buff_mem_req_info.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2;
		buff_mem_req_info.buffer = buffer;

	VkMemoryRequirements2 mem_req {};
		mem_req.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
	vkGetBufferMemoryRequirements2(logical_device, &buff_mem_req_info, &mem_req);

	VkMemoryAllocateInfo alloc_info {};
		alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		alloc_info.allocationSize = mem_req.memoryRequirements.size;
		alloc_info.memoryTypeIndex = get_memory_type(mem_req.memoryRequirements.memoryTypeBits, properties);

	Debug::vkresult_check(
		vkAllocateMemory(logical_device, &alloc_info, nullptr, &buffer_memory),
	__FILE__, __LINE__, __FUNCTION__);

	VkBindBufferMemoryInfo bind_buff_mem_info {};
		bind_buff_mem_info.sType = VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO;
		bind_buff_mem_info.buffer = buffer;
		bind_buff_mem_info.memory = buffer_memory;
		bind_buff_mem_info.memoryOffset = 0;

	vkBindBufferMemory2(logical_device, 1, &bind_buff_mem_info);
}

void MemDev::load_obj(const char *obj_path) {
	tinyobj::attrib_t attrib;
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	string warn, err;

	if ( !tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, obj_path)) {
		throw std::runtime_error(warn + err);
	}

	unordered_map<Vertex, uint32_t> vertices_map {};
	vector<Vertex> vertices_tmp {};
	vector<GLushort> indices_tmp {};

	for (const auto &shape : shapes) {
		for (const auto &index : shape.mesh.indices) {
			Vertex vertex {};

			vertex.pos = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};

			vertex.tex_coord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};

			vertex.color = {1.0f, 1.0f, 1.0f};

			if (vertices_map.count(vertex) == 0) {
				vertices_map[vertex] = static_cast<uint32_t>(vertices_tmp.size());
				vertices_tmp.push_back(vertex);

			}

			indices_tmp.push_back(vertices_map[vertex]);
		}
	}

	vertices_indices.push_back(make_pair(vertices_tmp, indices_tmp));
}

vector< pair<VkBuffer, VkDeviceMemory> > & MemDev::create_vertex_buffer(const VkDevice &logical_device, const VkQueue &graphics_queue) {

	const size_t vert_ind_size {vertices_indices.size()};
	vertex_buffer_n_mem.resize(vert_ind_size);

	for (size_t i = 0; i < vert_ind_size; i += 1) {
		VkDeviceSize buffer_size {sizeof(vertices_indices[i].first[0]) * vertices_indices[i].first.size()};
		VkBuffer staging_buffer {};
		VkDeviceMemory staging_buffer_memory {};
		create_buffer(logical_device, buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, staging_buffer, staging_buffer_memory);

		void *data {};
		vkMapMemory(logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
			memcpy_s(data, static_cast<size_t>(buffer_size), vertices_indices[i].first.data(), static_cast<size_t>(buffer_size));
		vkUnmapMemory(logical_device, staging_buffer_memory);

		create_buffer(logical_device, buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertex_buffer_n_mem[i].first, vertex_buffer_n_mem[i].second);

		copy_buffer(staging_buffer, vertex_buffer_n_mem[i].first, buffer_size, graphics_queue);

		vkDestroyBuffer(logical_device, staging_buffer, nullptr);
		vkFreeMemory(logical_device, staging_buffer_memory, nullptr);
	}

	return vertex_buffer_n_mem;
}

vector< pair<VkBuffer, VkDeviceMemory> > & MemDev::create_index_buffer(const VkDevice &logical_device, const VkQueue &graphics_queue) {

	const size_t vert_ind_size {vertices_indices.size()};
	index_buffer_n_mem.resize(vert_ind_size);

	for (size_t i = 0; i < vert_ind_size; i += 1) {
		VkDeviceSize buffer_size {sizeof(vertices_indices[i].second[0]) * vertices_indices[i].second.size()};
		VkBuffer staging_buffer {};
		VkDeviceMemory staging_buffer_memory {};
		create_buffer(logical_device, buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, staging_buffer, staging_buffer_memory);

		void *data {};
		vkMapMemory(logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
			memcpy_s(data, static_cast<size_t>(buffer_size), vertices_indices[i].second.data(), static_cast<size_t>(buffer_size));
		vkUnmapMemory(logical_device, staging_buffer_memory);

		create_buffer(logical_device, buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, index_buffer_n_mem[i].first, index_buffer_n_mem[i].second);

		copy_buffer(staging_buffer, index_buffer_n_mem[i].first, buffer_size, graphics_queue);

		vkDestroyBuffer(logical_device, staging_buffer, nullptr);
		vkFreeMemory(logical_device, staging_buffer_memory, nullptr);
	}

	return index_buffer_n_mem;
}

void MemDev::copy_buffer(const VkBuffer &src_buffer, VkBuffer &dst_buffer, VkDeviceSize size, const VkQueue &graphics_queue) {

	VkCommandBuffer command_buffer {MemDev::create_command_buffer()};

	VkBufferCopy2 copy_region {};
		copy_region.sType = VK_STRUCTURE_TYPE_BUFFER_COPY_2;
		copy_region.size = size;

	VkCopyBufferInfo2 copy_buffer_info {};
		copy_buffer_info.sType = VK_STRUCTURE_TYPE_COPY_BUFFER_INFO_2;
		copy_buffer_info.srcBuffer = src_buffer;
		copy_buffer_info.dstBuffer = dst_buffer;
		copy_buffer_info.regionCount = 1;
		copy_buffer_info.pRegions = &copy_region;

	vkCmdCopyBuffer2(command_buffer, &copy_buffer_info);

	end_command_buffer(graphics_queue);
}

GLuint MemDev::get_memory_type(const GLuint type_mask, const VkMemoryPropertyFlags mem_prop) {
	VkPhysicalDeviceMemoryProperties2 physical_mem_prop {};
		physical_mem_prop.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
	vkGetPhysicalDeviceMemoryProperties2(physical_device, &physical_mem_prop);

	for (GLuint i = 0; i < physical_mem_prop.memoryProperties.memoryTypeCount; i += 1) {
		if ((type_mask & (1 << i)) && (physical_mem_prop.memoryProperties.memoryTypes[i].propertyFlags & mem_prop) == mem_prop) {
			return i;
		}
	}

	throw runtime_error("failed to find suitable memory type!");
}

QueueFamilyIndices & MemDev::get_queue_families(const VkSurfaceKHR &surface) {
	GLuint queue_family_count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties2(physical_device, &queue_family_count, nullptr);

	vector<VkQueueFamilyProperties2> queue_families(queue_family_count);
	for (auto &family : queue_families) {
		family.sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
	}
	vkGetPhysicalDeviceQueueFamilyProperties2(physical_device, &queue_family_count, queue_families.data());

	int i = 0;
	for (const auto &family : queue_families) {
		if (family.queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			queue_indices.graphicsFamily = i;
		}

		VkBool32 present_support = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, surface, &present_support);

		if (present_support) {
			queue_indices.presentFamily = i;
		}

		if (queue_indices.is_complete()) {
			break;
		}

		i += 1;
	}

	return queue_indices;
}

VkFormat MemDev::get_depth_format() {
	const vector<VkFormat> formats {
		VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT
	};

	for (VkFormat format : formats) {
		VkFormatProperties2 props {};
			props.sType = VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2;

		vkGetPhysicalDeviceFormatProperties2(physical_device, format, &props);

		if ((props.formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) == VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
			return format;
		}
	}

	throw runtime_error("failed to find supported format!");
}

VkFormat MemDev::get_depth_stencil_format() {
	vector<VkFormat> formats = {
		VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM_S8_UINT,
	};

	for (VkFormat format : formats) {
		VkFormatProperties2 props {};
			props.sType = VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2;

		vkGetPhysicalDeviceFormatProperties2(physical_device, format, &props);

		if ((props.formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) == VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
			return format;
		}
	}

	throw runtime_error("failed to find supported format!");
}


pair<void *, VkDeviceSize> MemDev::create_uniform_buffer_aligned(const VkPhysicalDevice &physical_device, const size_t obj_count) {

	VkPhysicalDeviceProperties2 device_properties {};
	device_properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
	vkGetPhysicalDeviceProperties2(physical_device, &device_properties);

	VkDeviceSize min_ubo_alignment {device_properties.properties.limits.minUniformBufferOffsetAlignment};
	VkDeviceSize ubo_size {sizeof(UniformBufferObject)};

	if (min_ubo_alignment > 0) {
		ubo_size = (ubo_size + min_ubo_alignment - 1) & ~(min_ubo_alignment - 1);
	}

	VkDeviceSize buffer_size {obj_count * ubo_size};

	void *buffer {nullptr};
	int res = posix_memalign(&buffer, min_ubo_alignment, buffer_size);
	if (res != 0) {
		buffer = nullptr;
	}

	return make_pair(buffer, ubo_size);
}

void MemDev::create_image_memory_barrier(
	VkCommandBuffer command_buffer,
	VkImage image,
	VkAccessFlags src_access_mask,
	VkAccessFlags dst_access_mask,
	VkImageLayout old_image_layout,
	VkImageLayout new_image_layout,
	VkPipelineStageFlags src_stage_mask,
	VkPipelineStageFlags dst_stage_mask,
	VkImageSubresourceRange subresource_range) {

		VkImageMemoryBarrier2 image_memory_barrier {};
			image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
			image_memory_barrier.image = image;
			image_memory_barrier.srcAccessMask = src_access_mask;
			image_memory_barrier.dstAccessMask = dst_access_mask;
			image_memory_barrier.oldLayout = old_image_layout;
			image_memory_barrier.newLayout = new_image_layout;
			image_memory_barrier.dstStageMask = src_stage_mask;
			image_memory_barrier.dstStageMask = dst_stage_mask;
      image_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      image_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			image_memory_barrier.subresourceRange = subresource_range;

		VkDependencyInfo dependency_info {};
			dependency_info.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
			dependency_info.imageMemoryBarrierCount = 1;
			dependency_info.pImageMemoryBarriers = &image_memory_barrier;

		vkCmdPipelineBarrier2(command_buffer, &dependency_info);
}
