#include "Controller.hpp"

struct MainCharacter {
	float x {};
	float y {};
	float z {};
	float speed {0.002f};
};

struct Camera {
	float x {};
	float y {};
	float z {};
	vec3 front {vec3(0.0f, 0.0f, -1.0f)};
	vec3 up {vec3(0.0f, 1.0f,  0.0f)};
	float yaw {-90.0f}; // turned 90deg left because 0 turns 90deg to the right
	float pitch {0.0f};
	float zoom {45.0f};
	bool first_move {true};
	float prev_x {0.0f};
	float prev_y {0.0f};
	float speed {0.3f};
};

static bool pressed_keys[349] {false};
static MainCharacter main_char {};
static Camera camera {};

void Controller::rotate_object(
	const VkDevice &logical_device,
	const VkExtent2D &swapchain_extent,
	const GLuint current_image,
	vector<VkDeviceMemory> &swapchain_uniform_buffers_memory,
	pair<void *, VkDeviceSize> ubuffer_n_ubo_size_aligned,
	vector<GameObj> &game_objs) {

	const size_t obj_count = game_objs.size();

	// Main character
	game_objs[0].x = main_char.x;
	game_objs[0].y = main_char.y;
	game_objs[0].z = main_char.z;

	for (size_t i = 0; i < obj_count; i += 1) {
		auto *ubo = (UniformBufferObject *)((uint64_t) ubuffer_n_ubo_size_aligned.first + (i * ubuffer_n_ubo_size_aligned.second));

		ubo->projection = perspective(radians(camera.zoom), (GLfloat) swapchain_extent.width / (GLfloat) swapchain_extent.height, 0.1f, 100.0f);
		// Flip the Y-axis for Vulkan's clip coordinates
		ubo->projection[1][1] = ubo->projection[1][1] * (-1);

		vec3 camera_position {camera.x + 4.0f, camera.y + 4.0f, camera.z + 10.0f};
		ubo->view = lookAt(camera_position, camera_position + camera.front, camera.up);

		ubo->model = rotate(mat4(1.0f), radians(0.0f), vec3(1.0f, 0.0f, 0.0f)); // (x) y z

		for (size_t n = 0; n < obj_count; n += 1) {
			// Checking id to avoid self collision check
			if (game_objs[i].id != game_objs[n].id) {
				collision_check(game_objs[i], game_objs[n]);
			}
		}

		ubo->model = translate(ubo->model, vec3(game_objs[i].x, game_objs[i].y, game_objs[i].z));
	}

	VkDeviceSize buffer_size = ubuffer_n_ubo_size_aligned.second * obj_count;
	void *data {};
	vkMapMemory(logical_device, swapchain_uniform_buffers_memory[current_image], 0, buffer_size, 0, &data);
		memcpy_s(data, buffer_size, ubuffer_n_ubo_size_aligned.first, buffer_size);
	vkUnmapMemory(logical_device, swapchain_uniform_buffers_memory[current_image]);
	data = nullptr;
}

void Controller::collision_check(GameObj &obj1, GameObj &obj2) {

	vec3 obj1_center(obj1.x, obj1.y, obj1.z);
	float obj1_width_half {obj1.width / 88.0f / 2.0f };
	float obj1_height_half {obj1.height / 88.0f / 2.0f };
	float obj1_depth_half {obj1.depth / 88.0f / 2.0f };
	vec3 obj1_half_extents(obj1_width_half, obj1_height_half, obj1_depth_half);

	vec3 obj2_center(obj2.x, obj2.y, obj2.z);
	float obj2_width_half {obj2.width / 88.0f / 2.0f };
	float obj2_height_half {obj2.height / 88.0f / 2.0f };
	float obj2_depth_half {obj2.depth / 88.0f / 2.0f };
	vec3 obj2_half_extents(obj2_width_half, obj2_height_half, obj2_depth_half);

	vec3 distance_center {obj1_center - obj2_center};

	vec3 clamped {clamp(distance_center, -obj2_half_extents, obj2_half_extents)};

	vec3 obj2_closest_point {obj2_center + clamped};

	vec3 distance_vec {obj2_closest_point - obj1_center};

	bool touching {all(epsilonEqual(distance_vec, vec3(0.0f, 0.0f, 0.0f), 0.1f))};

	/* Resolving collisions */
	if (touching) {
		Direction direction {direction_check(distance_vec)};

		switch (direction) {
			case DOWN: {
				if (obj2.movable) {
					obj2.y += 0.003f;
				} else {
					main_char.y += main_char.speed;
				}
			} break;
			case LEFT: {
				if (obj2.movable) {
					obj2.x += 0.003f;
				} else {
					main_char.x += main_char.speed;
				}
			} break;
			case UP: {
				if (obj2.movable) {
					obj2.y -= 0.003f;
				} else {
					main_char.y -= main_char.speed;
				}
			} break;
			case RIGHT: {
				if (obj2.movable) {
					obj2.x -= 0.003f;
				} else {
					main_char.x -= main_char.speed;
				}
			} break;
			case BACK: {
				if (obj2.movable) {
					obj2.z += 0.003f;
				} else {
					main_char.z -= main_char.speed;
				}
			} break;
			case FRONT: {
				if (obj2.movable) {
				 obj2.z -= 0.003f;
				} else {
					main_char.z += main_char.speed;
				}
			} break;
		}
	}
}

Direction Controller::direction_check(const vec3 distance_vec) {
	vec3 direction_vecs[] {
		vec3(0.0f, 1.0f, 0.0f),  /* down */
		vec3(1.0f, 0.0f, 0.0f),  /* left */
		vec3(0.0f, -1.0f, 0.0f), /* up */
		vec3(-1.0f, 0.0f, 0.0f), /* right */ 
		vec3(0.0f, 0.0f, 1.0f),  /* back */
		vec3(0.0f, 0.0f, -1.0f)  /* front */
	};
	GLfloat max {0.0f};
	GLuint dir {0};

	for (GLuint n = 0; n < 6; n += 1) {
		GLfloat dot_product {glm::dot(glm::normalize(distance_vec), direction_vecs[n])};
		if (dot_product > max) {
			max = dot_product;
			dir = n;
		}
	}

	return (Direction) dir;
}

bool_array_349 & Controller::pressed_keys_array(void) {
	return pressed_keys;
}

void Controller::key_press_callback(GLFWwindow *window, const int key, const int scancode, const int action, const int mods) {
	(void) window;
	(void) scancode;
	(void) mods;

	if(action == GLFW_PRESS) {
		pressed_keys[key] = true;
	} else if(action == GLFW_RELEASE) {
		pressed_keys[key] = false;
	}
}

void Controller::key_action(const int key, GLFWwindow *window) {
	switch (key) {
		case GLFW_KEY_W: {
			camera.z -= main_char.speed;
		} break;
		case GLFW_KEY_A: {
			camera.x -= main_char.speed;
		} break;
		case GLFW_KEY_S: {
			camera.z += main_char.speed;
		} break;
		case GLFW_KEY_D: {
			camera.x += main_char.speed;
		} break;
		case GLFW_KEY_UP: {
			main_char.z -= main_char.speed;
		} break;
		case GLFW_KEY_LEFT: {
			main_char.x -= main_char.speed;
		} break;
		case GLFW_KEY_DOWN: {
			main_char.z += main_char.speed;
		} break;
		case GLFW_KEY_RIGHT: {
			main_char.x += main_char.speed;
		} break;
		case GLFW_KEY_Q: {
			camera.y -= main_char.speed;
		} break;
		case GLFW_KEY_E: {
			camera.y += main_char.speed;
		} break;
		case GLFW_KEY_R: {
			main_char.y += main_char.speed;
		} break;
		case GLFW_KEY_F: {
			main_char.y -= main_char.speed;
		} break;
		case GLFW_KEY_SPACE: {

		} break;
		case GLFW_KEY_ESCAPE: {
			glfwSetWindowShouldClose(window, true);
		} break;
		case GLFW_KEY_ENTER: {
			cout << GLFW_KEY_ENTER << endl;
		} break;
	}
}

void Controller::mouse_press_callback(GLFWwindow* window, const int button, const int action, const int mods) {
	(void) window;
	(void) mods;

	if(action == GLFW_PRESS) {
		switch (button) {
			case GLFW_MOUSE_BUTTON_LEFT:
				cout << "mouse left button" << endl;
			break;
			case GLFW_MOUSE_BUTTON_RIGHT:
				cout << "mouse right button" << endl;
			break;
		}
	}
}

void Controller::mouse_scroll_callback(GLFWwindow *window, const double scroll_x, const double scroll_y) {
	(void) window;
	(void) scroll_x;

	camera.zoom -= static_cast<float>(scroll_y);
	if (camera.zoom < 1.0f) {
			camera.zoom = 1.0f;
	} else if (camera.zoom > 45.0f) {
			camera.zoom = 45.0f;
	}
}

void Controller::mouse_position_callback(GLFWwindow *window, const double pos_x, const double pos_y) {
	(void) window;

	float mouse_x {static_cast<float>(pos_x)};
	float mouse_y {static_cast<float>(pos_y)};

	if (camera.first_move) {
		camera.prev_x = mouse_x;
		camera.prev_y = mouse_y;
		camera.first_move = false;
	}

	float offset_x {mouse_x - camera.prev_x};
	float offset_y {camera.prev_y - mouse_y}; // reversed y offset
	camera.prev_x = mouse_x;
	camera.prev_y = mouse_y;

	offset_x *= camera.speed;
	offset_y *= camera.speed;

	camera.yaw += offset_x;
	camera.pitch += offset_y;

	float camera_x {static_cast<float>(cos(radians(camera.yaw)) * cos(radians(camera.pitch)))};
	float camera_y {static_cast<float>(sin(radians(camera.pitch)))};
	float camera_z {static_cast<float>(sin(radians(camera.yaw)) * cos(radians(camera.pitch)))};
	camera.front = normalize(vec3(camera_x, camera_y, camera_z));
}