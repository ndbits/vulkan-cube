#include "Graphics.hpp"
#include "Controller.hpp"
#include "Music.hpp"

int main() {
try {

	Debug::check_validation_layer_support();
	pair<VkInstance &, GLFWwindow *> instance_n_window {Graphics::create_instance()};
	VkDebugUtilsMessengerEXT debug_messenger {Debug::create_messenger(instance_n_window.first)};
	VkPhysicalDevice physical_device {MemDev::pick_physical_device(instance_n_window.first)};
	GLuint device_mask {MemDev::get_device_mask(instance_n_window.first)};
	VkSurfaceKHR surface {Graphics::create_surface(instance_n_window.first, instance_n_window.second)};
	VkDevice logical_device {MemDev::create_logical_device(surface)};

	pair<VkQueue &, VkQueue &> graphics_n_present_queue {Graphics::get_graphics_present_queues(logical_device)};

	VkCommandPool command_pool {MemDev::create_command_pool(surface)};

	SwapchainSupportDetails swapchain_details = Graphics::get_swapchain_details(physical_device);
	tuple<VkSwapchainKHR, vector<VkImage> &, vector<VkImageView> &, ImageTriple &> swapchain_imgs_views_depthtriple {Graphics::create_swapchain(logical_device)};
	GLuint swapchain_image_count {static_cast<GLuint>(get<2>(swapchain_imgs_views_depthtriple).size())};

	VkExtent2D swapchain_extent {Graphics::choose_swapchain_extent(swapchain_details.capabilities)};

	VkSampler texsampler {Image::create_texsampler(physical_device, logical_device)};

  pair< vector<ImageTriple> &, vector<GameObj> &> teximage_triples_n_game_objs {Image::create_game_obj(
		logical_device,
		graphics_n_present_queue.first,
		"objects/cube.png",
		"objects/cube.obj",
		5,
		257.0f,
		257.0f,
		257.0f,
		true
	)};

	pair<void *, VkDeviceSize> ubuffer_n_ubo_size_aligned = MemDev::create_uniform_buffer_aligned(physical_device, teximage_triples_n_game_objs.first.size());

	vector< pair<VkBuffer, VkDeviceMemory> > vertex_buffer_n_mem {MemDev::create_vertex_buffer(logical_device, graphics_n_present_queue.first)};
	vector< pair<VkBuffer, VkDeviceMemory> > index_buffer_n_mem {MemDev::create_index_buffer(logical_device, graphics_n_present_queue.first)};

	VkDescriptorPool descriptor_pool {Image::create_descriptor_pool(logical_device, swapchain_image_count)};
	vector<VkDescriptorSetLayout> descriptor_layouts {Image::create_descriptor_set_layouts(logical_device)};

	pair<vector<VkBuffer> &, vector<VkDeviceMemory> &> swapchain_uniform_buff_n_mem {Image::create_uniform_buffers(logical_device, swapchain_image_count, teximage_triples_n_game_objs.first.size(), ubuffer_n_ubo_size_aligned.second)};

	vector<VkDescriptorSet> descriptor_sets {Image::create_descriptor_sets(logical_device, swapchain_image_count, ubuffer_n_ubo_size_aligned.second)};

	pair<VkPipeline, VkPipelineLayout> pipeline_n_layout {Graphics::create_pipeline(physical_device, logical_device, descriptor_layouts)};

	vector<VkCommandBuffer> swapchain_command_buffers {MemDev::create_swapchain_command_buffers(
		descriptor_sets,
		pipeline_n_layout.first,
		pipeline_n_layout.second,
		swapchain_extent,
		swapchain_image_count,
    ubuffer_n_ubo_size_aligned.second,
		swapchain_imgs_views_depthtriple)};

	const GLint max_frames_inflight {2};
	vector<VkSemaphore> image_available_semaphores(max_frames_inflight);
	vector<VkSemaphore> render_finished_semaphores(max_frames_inflight);
	vector<VkFence> in_flight_fences(max_frames_inflight);
	vector<VkFence> images_in_flight(swapchain_image_count, VK_NULL_HANDLE);
	size_t current_frame = 0;

	VkSemaphoreCreateInfo semaphore_info {};
	semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fence_info {};
	fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (size_t i = 0; i < max_frames_inflight; i += 1) {
		Debug::vkresult_check(
			vkCreateSemaphore(logical_device, &semaphore_info, nullptr, &image_available_semaphores[i]),
		__FILE__, __LINE__, __FUNCTION__);

		Debug::vkresult_check(
			vkCreateSemaphore(logical_device, &semaphore_info, nullptr, &render_finished_semaphores[i]),
		__FILE__, __LINE__, __FUNCTION__);

		Debug::vkresult_check(
			vkCreateFence(logical_device, &fence_info, nullptr, &in_flight_fences[i]),
		__FILE__, __LINE__, __FUNCTION__);
	}

	// thread music_thread(&Music::play);
	// music_thread.detach();

	glfwSetKeyCallback(instance_n_window.second, Controller::key_press_callback);
	glfwSetCursorPosCallback(instance_n_window.second, Controller::mouse_position_callback);
	glfwSetMouseButtonCallback(instance_n_window.second, Controller::mouse_press_callback);
	glfwSetScrollCallback(instance_n_window.second, Controller::mouse_scroll_callback);

	while ( !glfwWindowShouldClose(instance_n_window.second)) {
		glfwPollEvents();

		for(int i = 0, n = sizeof(Controller::pressed_keys_array()); i < n; i += 1) {
			if( !Controller::pressed_keys_array()[i]) continue;

			Controller::key_action(i, instance_n_window.second);
		}

		vkWaitForFences(logical_device, 1, &in_flight_fences[current_frame], VK_TRUE, UINT64_MAX);

		VkAcquireNextImageInfoKHR acquire_next_image_info {};
			acquire_next_image_info.sType = VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR;
			acquire_next_image_info.swapchain = get<0>(swapchain_imgs_views_depthtriple);
			acquire_next_image_info.timeout = UINT64_MAX;
			acquire_next_image_info.semaphore = image_available_semaphores[current_frame];
			acquire_next_image_info.fence = VK_NULL_HANDLE;
			acquire_next_image_info.deviceMask = device_mask;

		GLuint image_index {};
		Debug::vkresult_check(
			vkAcquireNextImage2KHR(logical_device, &acquire_next_image_info, &image_index),
		__FILE__, __LINE__, __FUNCTION__);

		Controller::rotate_object(
			logical_device,
			swapchain_extent,
			image_index,
			swapchain_uniform_buff_n_mem.second,
			ubuffer_n_ubo_size_aligned,
			teximage_triples_n_game_objs.second);

		if (images_in_flight[image_index] != VK_NULL_HANDLE) {
			vkWaitForFences(logical_device, 1, &images_in_flight[image_index], VK_TRUE, UINT64_MAX);
		}
		images_in_flight[image_index] = in_flight_fences[current_frame];

		VkSemaphoreSubmitInfo semaphore_submit_info {};
			semaphore_submit_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
			semaphore_submit_info.semaphore = image_available_semaphores[current_frame];
			semaphore_submit_info.stageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

		VkCommandBufferSubmitInfo command_buff_submit_info {};
			command_buff_submit_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
			command_buff_submit_info.commandBuffer = swapchain_command_buffers[image_index];

		VkSemaphoreSubmitInfo signal_semaphore_submit_info {};
			signal_semaphore_submit_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
			signal_semaphore_submit_info.semaphore = render_finished_semaphores[current_frame];

		VkSubmitInfo2 submit_info {};
			submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
			submit_info.waitSemaphoreInfoCount = 1;
			submit_info.pWaitSemaphoreInfos = &semaphore_submit_info;
			submit_info.commandBufferInfoCount = 1;
			submit_info.pCommandBufferInfos = &command_buff_submit_info;
			submit_info.signalSemaphoreInfoCount = 1;
			submit_info.pSignalSemaphoreInfos = &signal_semaphore_submit_info;

		vkResetFences(logical_device, 1, &in_flight_fences[current_frame]);

		Debug::vkresult_check(
			vkQueueSubmit2(graphics_n_present_queue.first, 1, &submit_info, in_flight_fences[current_frame]),
		__FILE__, __LINE__, __FUNCTION__);

		VkPresentInfoKHR present_info {};
			present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			present_info.waitSemaphoreCount = 1;
			VkSemaphore signal_semaphores[] {render_finished_semaphores[current_frame]};
			present_info.pWaitSemaphores = signal_semaphores;
			present_info.swapchainCount = 1;
			VkSwapchainKHR swapchains[] { get<0>(swapchain_imgs_views_depthtriple) };
			present_info.pSwapchains = swapchains;
			present_info.pImageIndices = &image_index;

		Debug::vkresult_check(
			vkQueuePresentKHR(graphics_n_present_queue.second, &present_info),
		__FILE__, __LINE__, __FUNCTION__);

		current_frame = (current_frame + 1) % max_frames_inflight;

	}

	/* Clean up */
	if (vkDeviceWaitIdle(logical_device) == VK_SUCCESS) {
		for (GLint i = 0; i < max_frames_inflight; i += 1) {
			vkDestroySemaphore(logical_device, render_finished_semaphores[i], nullptr);
			vkDestroySemaphore(logical_device, image_available_semaphores[i], nullptr);
			vkDestroyFence(logical_device, in_flight_fences[i], nullptr);
		}

		vkFreeCommandBuffers(logical_device, command_pool, static_cast<GLuint>(swapchain_command_buffers.size()), swapchain_command_buffers.data());
		vkDestroyPipelineLayout(logical_device, pipeline_n_layout.second, nullptr);
		vkDestroyPipeline(logical_device, pipeline_n_layout.first, nullptr);

		for (size_t i = 0; i < swapchain_image_count; i += 1) {
			vkDestroyBuffer(logical_device, swapchain_uniform_buff_n_mem.first[i], nullptr);
			vkFreeMemory(logical_device, swapchain_uniform_buff_n_mem.second[i], nullptr);
		}
		for (size_t i = 0, n = descriptor_layouts.size(); i < n; i += 1) {
			vkDestroyDescriptorSetLayout(logical_device, descriptor_layouts[i], nullptr);
		}
		vkDestroyDescriptorPool(logical_device, descriptor_pool, nullptr);

		for (size_t i = 0, n = index_buffer_n_mem.size(); i < n; i += 1) {
			vkDestroyBuffer(logical_device, index_buffer_n_mem[i].first, nullptr);
			vkFreeMemory(logical_device, index_buffer_n_mem[i].second, nullptr);
			vkDestroyBuffer(logical_device, vertex_buffer_n_mem[i].first, nullptr);
			vkFreeMemory(logical_device, vertex_buffer_n_mem[i].second, nullptr);
		}

		/* Destroying uniform buffer */
		free(ubuffer_n_ubo_size_aligned.first);

		/* Destroying textures */
		for (size_t i = 0, n = teximage_triples_n_game_objs.first.size(); i < n; i += 1) {
			vkDestroyImageView(logical_device, teximage_triples_n_game_objs.first[i].view, nullptr);
			vkDestroyImage(logical_device, teximage_triples_n_game_objs.first[i].image, nullptr);
			vkFreeMemory(logical_device, teximage_triples_n_game_objs.first[i].mem, nullptr);
		}

		/* Destroying depth triple */
		vkDestroyImageView(logical_device, get<3>(swapchain_imgs_views_depthtriple).view, nullptr);
		vkDestroyImage(logical_device, get<3>(swapchain_imgs_views_depthtriple).image, nullptr);
		vkFreeMemory(logical_device, get<3>(swapchain_imgs_views_depthtriple).mem, nullptr);

		/* Destroying swapchain views */
		for (size_t i = 0; i < get<2>(swapchain_imgs_views_depthtriple).size(); i += 1) {
			vkDestroyImageView(logical_device, get<2>(swapchain_imgs_views_depthtriple)[i], nullptr);
		}
		vkDestroySampler(logical_device, texsampler, nullptr);

		vkDestroySwapchainKHR(logical_device, get<0>(swapchain_imgs_views_depthtriple), nullptr);

		vkDestroyCommandPool(logical_device, command_pool, nullptr);

		vkDestroyDevice(logical_device, nullptr);
		vkDestroySurfaceKHR(instance_n_window.first, surface, nullptr);
		Debug::destroy_messenger(instance_n_window.first, debug_messenger);
		vkDestroyInstance(instance_n_window.first, nullptr);
	}

	glfwDestroyWindow(instance_n_window.second);
	glfwTerminate();
} catch (const exception &e) {
	cerr << e.what() << endl;
	return EXIT_FAILURE;
}

return EXIT_SUCCESS;
}