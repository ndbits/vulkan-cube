#pragma once

#include "Debug.hpp"

struct ImageTriple {
	VkImage image;
	VkImageView view;
	VkDeviceMemory mem;
};

struct UniformBufferObject {
	alignas(16) mat4 projection {};
	alignas(16) mat4 view {};
	alignas(16) mat4 model {};
};

struct QueueFamilyIndices {
	optional<GLuint> graphicsFamily;
	optional<GLuint> presentFamily;

	GLboolean is_complete() {
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

struct Vertex {
	glm::vec3 pos;
	glm::vec3 color;
	glm::vec2 tex_coord;

	static VkVertexInputBindingDescription get_binding_description() {
		VkVertexInputBindingDescription binding_description {};
		binding_description.binding = 0;
		binding_description.stride = sizeof(Vertex);
		binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return binding_description;
	}

	static array<VkVertexInputAttributeDescription, 3> get_attribute_descriptions() {
		array<VkVertexInputAttributeDescription, 3> attribute_descriptions {};
			attribute_descriptions[0].binding = 0;
			attribute_descriptions[0].location = 0;
			attribute_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
			attribute_descriptions[0].offset = offsetof(Vertex, pos);

			attribute_descriptions[1].binding = 0;
			attribute_descriptions[1].location = 1;
			attribute_descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
			attribute_descriptions[1].offset = offsetof(Vertex, color);

			attribute_descriptions[2].binding = 0;
			attribute_descriptions[2].location = 2;
			attribute_descriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
			attribute_descriptions[2].offset = offsetof(Vertex, tex_coord);

		return attribute_descriptions;
	}

	bool operator==(const Vertex &other) const {
		return pos == other.pos && color == other.color && tex_coord == other.tex_coord;
	}
};

namespace std {
	template<> struct hash<Vertex> {
		size_t operator()(Vertex const &vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^ (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^ (hash<glm::vec2>()(vertex.tex_coord) << 1);
		}
	};
}

class MemDev {
public:

	static VkPhysicalDevice & pick_physical_device(const VkInstance &instance);
	static GLuint get_device_mask(const VkInstance &instance);
	static VkDevice & create_logical_device(const VkSurfaceKHR &surface);

	static VkCommandPool & create_command_pool(const VkSurfaceKHR &surface);
	static VkCommandBuffer & create_command_buffer(void);
	static void end_command_buffer(const VkQueue &graphics_queue);
	static vector<VkCommandBuffer> & create_swapchain_command_buffers(
		const vector<VkDescriptorSet> &descriptor_sets,
		const VkPipeline &graphics_pipeline,
		const VkPipelineLayout &graphics_pipeline_layout,
		const VkExtent2D &swapchain_extent,
		const GLuint swapchain_image_count,
    const VkDeviceSize ubo_size_aligned,
		tuple<VkSwapchainKHR, vector<VkImage> &, vector<VkImageView> &, ImageTriple &> swapchain_imgs_views_depthtriple);

	static void create_buffer(
		const VkDevice &logical_device,
		const VkDeviceSize size,
		const VkBufferUsageFlags usage,
		const VkMemoryPropertyFlags properties,
		VkBuffer &buffer,
		VkDeviceMemory &buffer_memory);

	static void load_obj(const char *obj_path);

	static vector< pair<VkBuffer, VkDeviceMemory> > & create_vertex_buffer(const VkDevice &logical_device, const VkQueue &graphics_queue);
	static vector< pair<VkBuffer, VkDeviceMemory> > & create_index_buffer(const VkDevice &logical_device, const VkQueue &graphics_queue);

	static void copy_buffer(const VkBuffer &src_buffer, VkBuffer &dst_buffer, VkDeviceSize size, const VkQueue &graphics_queue);

	static GLuint get_memory_type(const GLuint type_mask, const VkMemoryPropertyFlags mem_prop);
	static QueueFamilyIndices & get_queue_families(const VkSurfaceKHR &surface);
	static VkFormat get_depth_format(void);
	static VkFormat get_depth_stencil_format(void);

	static pair<void *, VkDeviceSize> create_uniform_buffer_aligned(const VkPhysicalDevice &physical_device, const size_t obj_count);

private:

	static void create_image_memory_barrier(
		VkCommandBuffer command_buffer,
		VkImage image,
		VkAccessFlags src_access_mask,
		VkAccessFlags dst_access_mask,
		VkImageLayout old_image_layout,
		VkImageLayout new_image_layout,
		VkPipelineStageFlags src_stage_mask,
		VkPipelineStageFlags dst_stage_mask,
		VkImageSubresourceRange subresource_range);
};