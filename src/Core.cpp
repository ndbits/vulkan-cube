#include "Core.hpp"

vector<char> Core::read_file(const char *filename) {
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if ( !file.is_open()) {
		throw runtime_error("failed to open file!");
	}

	size_t file_size = (size_t) file.tellg();
	vector<char> buffer(file_size);

	file.seekg(0);
	file.read(buffer.data(), file_size);

	file.close();

	return buffer;
}