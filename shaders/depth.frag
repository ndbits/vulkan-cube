#version 460

layout(set = 0, binding = 0) uniform texture2D textures;
layout(set = 0, binding = 1) uniform sampler tex_sampler;

layout(location = 0) in vec3 frag_color;
layout(location = 1) in vec2 frag_tex_coord;

layout(location = 0) out vec4 out_color;

void main() {
    out_color = texture(sampler2D(textures, tex_sampler), frag_tex_coord);
}
