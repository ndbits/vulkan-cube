#!/usr/bin/env zsh

RED='\033[0;31m'
GREEN='\033[0;32m'
NONE='\033[0m'
PURPLE='\033[0;35m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
BOLD='\033[1m'
UNDERLINE='\033[4m'

declare -A compile_cpp
declare -A osx_conf
declare -A arch_conf
declare -A deb_conf

local verbose=0
local force=0
local shaders=0

### Spectre mitigations
security_mitigations=' -mretpoline -mspeculative-load-hardening -fcf-protection' # -fsanitize=safe-stack

# MAC OS ------------------------------------------------------------------------------------------------------------
osx_conf[shaders_dir]='shaders'
osx_conf[cflags]="-flto=thin -std=c++20 --target=x86_64-apple-macosx13 -O3 -fpie ${security_mitigations} -Wall -Wextra -ftrapv"
osx_conf[include_dirs]='-Isrc -Ilibs -I/usr/local/include/MoltenVK -Ilibs/glfw-3.4/include -Ilibs/glm-1.0.1 -Ilibs/libsafec-3.8.1/include -Ilibs/portaudio-19.7.0/include'
osx_conf[ldflags]='--target=x86_64-apple-macosx13 -flto=thin -L/usr/local/lib -Wl,-rpath,/usr/local/lib -lvulkan -framework Cocoa -framework IOKit -framework CoreAudio -framework AudioToolbox -lpthread'
osx_conf[static_libs]='libs/libsafec-3.8.1/osx/libsafec.a libs/glfw-3.4/osx/lib-x86_64/libglfw3.a libs/portaudio-19.7.0/osx/libportaudio.a'
osx_conf[src_dirs]='src'
osx_debug_cflags="${osx_conf[cflags]} -ggdb -fno-omit-frame-pointer" # -fsanitize=address -fsanitize=undefined"
osx_debug_ldflags=${osx_conf[ldflags]}

# Arch Linux --------------------------------------------------------------------------------------------------------
arch_conf[shaders_dir]='shaders'
arch_conf[cflags]="-flto=thin -std=c++23 --target=x86_64-pc-linux-gnu -O3 -fpie ${security_mitigations} -Wall -Wextra -ftrapv"
arch_conf[include_dirs]='-Isrc -Ilibs -I/usr/include -I/opt/vulkansdk/x86_64/include -Ilibs/glfw-3.4/include -Ilibs/glm-1.0.1 -Ilibs/libsafec-3.8.1/include -Ilibs/portaudio-19.7.0/include'
arch_conf[ldflags]='--target=x86_64-pc-linux-gnu -fuse-ld=lld -flto=thin -demangle -L/usr/lib -L/opt/vulkansdk/x86_64/lib -lvulkan -lrt -lm -lpthread'
arch_conf[static_libs]='libs/libsafec-3.8.1/linux/libsafec.a libs/glfw-3.4/linux/lib-x86_64/libglfw3.a libs/portaudio-19.7.0/linux/libportaudio.a'
arch_conf[src_dirs]='src'
arch_debug_cflags="${arch_conf[cflags]} -ggdb -fno-omit-frame-pointer -fsanitize=address -fsanitize=undefined"
arch_debug_ldflags="-lasan -lubsan ${arch_conf[ldflags]}"

# Debian Linux ------------------------------------------------------------------------------------------------------
deb_conf[shaders_dir]='shaders'
deb_conf[cflags]="-flto=thin -std=c++23 --target=x86_64-pc-linux-gnu -O3 -fpie ${security_mitigations} -Wall -Wextra -ftrapv"
deb_conf[include_dirs]='-Isrc -Ilibs -I/usr/include -I/opt/vulkansdk/x86_64/include -Ilibs/glfw-3.4/include -Ilibs/glm-1.0.1 -Ilibs/libsafec-3.8.1/include -Ilibs/portaudio-19.7.0/include'
deb_conf[ldflags]='--target=x86_64-pc-linux-gnu -fuse-ld=lld -flto=thin -demangle -L/usr/lib -L/opt/vulkansdk/x86_64/lib -lvulkan -lrt -lm -lpthread'
deb_conf[static_libs]='/usr/lib/llvm-18/lib/libLLVMLTO.a libs/libsafec-3.8.1/linux/libsafec.a libs/glfw-3.4/linux/lib-x86_64/libglfw3.a libs/portaudio-19.7.0/linux/libportaudio.a'
deb_conf[src_dirs]='src'
deb_debug_cflags="${deb_conf[cflags]} -ggdb -fno-omit-frame-pointer -fsanitize=address -fsanitize=undefined"
deb_debug_ldflags="-lasan -lubsan ${deb_conf[ldflags]}"

### Sanitize and run a command
# run_cmd "cmd"
run_cmd() {
	echo -n $(zsh -c "${1/[^a-zA-Z0-9_ -.,=\\\/\{\}]/}")
}

count_cpus() {
	if [[ "$OSTYPE" == "darwin"* ]]; then
		echo -n $(sysctl -n hw.ncpu)
	elif [[ "$OSTYPE" == "linux-gnu"* || "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]; then
		echo -n $(nproc --all)
	fi
}
local num_cpus=$(count_cpus)

### Returns b3 checksum for a file
b3_checksum() {
	run_cmd "b3sum $1"
}

### Finds cpp files for hpp files that were included and adds these cpp to compile_cpp[] but stops recursion on first cpp occurance.
get_cpp() {
	local file=$1 && shift
	local src_dirs="$@"
	if [[ $file == *.(hpp|h) ]]; then
		# chop dir/ from file
		local xpp=("${(s|/|)file}")
		local grep_arr=($(run_cmd "grep --directories=skip --devices=skip -o --include=\*.{cpp,hpp,h} ${xpp[2]} $src_dirs"))

		for fl in "${grep_arr[@]}"; do
			local found=("${(s/:/)fl}")
			if [[ $found[1] == *.(hpp|h) ]]; then
				get_cpp $found[1] $src_dirs
			elif [[ $found[1] == *.cpp ]]; then
				compile_cpp[${found[1]}]=${found[1]}
			fi
		done
	else
		compile_cpp[$file]=$file
	fi
}

### compile_bin 'osx_conf' 'build/release_objects' 'build/Cube.bin'
compile_bin() {
	local shaders_dir=${${(P)1}[shaders_dir]}
	local cflags=${${(P)1}[cflags]}
	local include_dirs=${${(P)1}[include_dirs]}
	local ldflags=${${(P)1}[ldflags]}
	local static_libs=${${(P)1}[static_libs]}
	local src_dirs=${${(P)1}[src_dirs]}
	src_dirs=(${(s/ /)src_dirs})
	local objs_dir=$2
	local bin_file=$3

	if [[ $force -eq 1 ]]; then
		rm -rf './build'
		rm -rf 'shaders/checksums_shaders.txt'
	fi

	if [[ $shaders -eq 1 ]]; then
		rm -rf 'shaders/checksums_shaders.txt'
	fi

	mkdir -p $objs_dir

	if [[ -f ${objs_dir}/checksums_src.txt ]]; then
		local ccobj_checksums=$(< ${objs_dir}/checksums_src.txt)
		local checksums_arr=("${(f)ccobj_checksums}")
		unset ccobj_checksums
	else
		local checksums_arr=()
	fi

	local checksums_tmp=''
	local cpp_objects=''

	local checksum_compile_sh=$(b3_checksum compile.sh)
	if [[ $checksum_compile_sh != $checksums_arr[1] ]]; then
		checksums_tmp+="${checksum_compile_sh}\n"
		for dir in "${src_dirs[@]}"; do
			for file in $dir/*.(cpp|hpp|h|)(.); do
				if [[ $file == *.cpp ]]; then
					local obj_file=("${objs_dir}/${file:t:r}.o")
					cpp_objects+="${obj_file} "
					compile_cpp[$file]=${file}
				fi
				local file_checksum=$(b3_checksum $file)
				checksums_tmp+="${file_checksum}\n"
			done
		done
	else
		checksums_arr=("${checksums_arr[@]:1}")
		for dir in "${src_dirs[@]}"; do
			for file in $dir/*.(cpp|hpp|h|)(.); do
				if [[ $file == *.cpp ]]; then
					local obj_file=("${objs_dir}/${file:t:r}.o")
					cpp_objects+="${obj_file} "
				fi

				local checksum=$(b3_checksum $file)

				if [[ $#checksums_arr > 0 && $checksum != $checksums_arr[1] || $#checksums_arr == 0 || ! -f $obj_file ]]; then
					get_cpp $file "${src_dirs[@]/%//*}"
				fi
				checksums_tmp+="${checksum}\n"
				checksums_arr=("${checksums_arr[@]:1}")
			done
		done
		checksums_tmp="${checksum_compile_sh}\n${checksums_tmp}"
	fi

	# Parallel compilation of objects on each CPU if cpp files changed
	# key & val are the same cpp file
	(
	for key val in "${(@kv)compile_cpp}"; do
		((i=i%$num_cpus)); ((i++==0)) && wait
		local compile_obj=("${objs_dir}/${${val#*\/}%\.*}.o")
		if [[ $verbose -eq 0 ]]; then
			echo -e "${GREEN}Compiling:${NONE} ${RED}${val}${NONE}"
		else
			echo -e "${GREEN}Compiling:${NONE} clang++ -c ${CYAN}${cflags}${NONE} ${PURPLE}${include_dirs}${NONE} ${RED}${val}${NONE} -o ${BLUE}${compile_obj}${NONE}"
		fi
		run_cmd "clang++ -c ${cflags} ${include_dirs} ${val} -o ${compile_obj}" &
	done
		# Waits for the batch of objects to be compiled so it can load another batch.
		wait
	)

	if [[ $#compile_cpp > 0 ]]; then
		# linking objects with bin and libs
		if [[ $verbose -eq 0 ]]; then
			echo -e "${GREEN}Linking:${NONE} ${CYAN}${bin_file}${NONE}"
		else
			echo -e "${GREEN}Linking:${NONE} clang++ ${CYAN}${ldflags}${NONE} ${RED}${static_libs}${NONE} ${BLUE}${cpp_objects}${NONE} -o ${PURPLE}${bin_file}${NONE}"
		fi
		run_cmd "clang++ ${ldflags} ${static_libs} ${cpp_objects} -o ${bin_file}"
		echo -n "$checksums_tmp" > ${objs_dir}/checksums_src.txt
	fi

	compile_shaders "$shaders_dir"
}

### compile_shaders "$shaders_dir"
compile_shaders() {
	local shaders_dir=$1

	mkdir -p $shaders_dir

	local checksums_shaders_tmp=''

	if [[ -f ${shaders_dir}/checksums_shaders.txt ]]; then
		local sums_shaders=$(< ${shaders_dir}/checksums_shaders.txt)
		local checksums_shaders_arr=("${(f)sums_shaders}")
		unset sums_shaders
	else
		local checksums_shaders_arr=()
	fi

	# Parallel compilation of shaders on each CPU
	(
	for shader in $shaders_dir/*.(frag|vert)(.); do
		((i=i%$num_cpus)); ((i++==0)) && wait
		local shader_checksum=$(b3_checksum $shader)
		local spirv_file="${shader//./_}.spv"

		if [[ $#checksums_shaders_arr > 0 &&
			$shader_checksum != $checksums_shaders_arr[1] ||
			$#checksums_shaders_arr == 0 || ! -f $spirv_file ]]; then

			if [[ $verbose -eq 0 ]]; then
				echo -e "${GREEN}Compiling Shader:${NONE} ${RED}${shader}${NONE}"
			else
				echo -e "${GREEN}Compiling Shader:${NONE} glslc -std=460 --target-env=vulkan1.1 --target-spv=spv1.3 ${PURPLE}${shader}${NONE} -o ${GREEN}${spirv_file}${NONE}"
			fi
			run_cmd "glslc -std=460 --target-env=vulkan1.1 --target-spv=spv1.3 ${shader} -o ${spirv_file}" &
		fi
		checksums_shaders_tmp+="${shader_checksum}\n"
		checksums_shaders_arr=("${checksums_shaders_arr[@]:1}")
	done
		echo -n "$checksums_shaders_tmp" > ${shaders_dir}/checksums_shaders.txt
	)
}

case "$1" in
	--help | -h)

		local help_msg="\
${BOLD}DESCRIPTION${NONE}
       Compiles only changed files.

${BOLD}OPTIONS${NONE}
       -h
           This help message.

       -v
           Verbose output.

       -l
           Check compiled app for memory leaks.

       -d
           Builds a debug build of the app.

       -f
           Removes build directory and shaders checksums to recompile all files.

       -s
           Recompiles shaders.

${BOLD}Vulkan Commands${NONE}
       vkvia
           Vulkan Installation Analyzer.

       vulkaninfo
           Vulkan Info.
"

		echo -e "$help_msg" | less -R

	;;
	-l)

		echo "Checking compiled app for memory leaks..."
		run_cmd "valgrind --tool=memcheck --leak-check=full --show-reachable=yes build/Cube-debug.bin"

	;;
	-d)
		force=1
		if [[ "$OSTYPE" == "darwin"* ]]; then
			osx_conf[cflags]=$osx_debug_cflags
			osx_conf[ldflags]=$osx_debug_ldflags
			compile_bin 'osx_conf' 'build/debug_objects' 'build/Cube-debug.bin'

		elif [[ "$OSTYPE" == "linux-gnu"* ]]; then

			if [[ -f "/etc/debian_version" ]]; then
				deb_conf[cflags]=$deb_debug_cflags
				deb_conf[ldflags]=$deb_debug_ldflags
				compile_bin 'linux_conf' 'build/debug_objects' 'build/Cube-debug.bin'
			elif [[ -f "/bin/pacman" ]]; then
				arch_conf[cflags]=$arch_debug_cflags
				arch_conf[ldflags]=$arch_debug_ldflags
				compile_bin 'arch_conf' 'build/debug_objects' 'build/Cube-debug.bin'
			fi
		fi

	;;
	*)

		case "$1" in
			-v) verbose=1;;
			-f) force=1;;
			-s) shaders=1;;
		esac

		if [[ "$OSTYPE" == 'darwin'* ]]; then
			### Release build
			compile_bin 'osx_conf' 'build/release_objects' 'build/Cube.bin'

		elif [[ "$OSTYPE" == 'linux-gnu'* ]]; then

			if [[ -f "/etc/debian_version" ]]; then
				compile_bin 'linux_conf' 'build/release_objects' 'build/Cube.bin'
			elif [[ -f "/bin/pacman" ]]; then
				compile_bin 'arch_conf' 'build/release_objects' 'build/Cube.bin'
			fi
		fi
	;;
esac
